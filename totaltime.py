
all_dict = {}

for item in open( 'readus.md', 'r' ):
	arr = item.split()
	if len(arr) > 0:
		if not all_dict.get( arr[0], False ):
			all_dict[ arr[0] ] = float( arr[1] )
		else:
			all_dict[ arr[0] ] += float( arr[1] )

total_time = sum( all_dict.values() )
print( 'total time : ' + str( total_time ) )
print( '------' )

for k,v in all_dict.items():
	print( k + ' : ' + str(v) + ',\t' + str(round(v*100/total_time)) + '%' )
