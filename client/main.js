import { Meteor } from 'meteor/meteor';
import React from 'react';
import { render } from 'react-dom';
import { renderRoutes } from '../imports/routes.js';
//import '../imports/ui/template.js';

Meteor.startup(() => {
  render(renderRoutes(), document.getElementById('app'));
});
