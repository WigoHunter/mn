module.exports = {
  servers: {
    one: {
      host: '139.162.12.156',
      username: 'root',
      // pem:
      password: 'Tweb2017'
      // or leave blank for authenticate from ssh-agent
    }
  },

  meteor: {
    name: 'MusicNext',
    path: '../',
    'volume': {
      '/images': '/images'
    },
    servers: {
      one: {},
    },
    buildOptions: {
      serverOnly: true,
    },
    env: {
      ROOT_URL: 'http://139.162.12.156',
      MONGO_URL: 'mongodb://localhost/meteor',
    },

    // change to 'kadirahq/meteord' if your app is not using Meteor 1.4
    dockerImage: 'abernix/meteord:base',
    deployCheckWaitTime: 120,

    // Show progress bar while uploading bundle to server
    // You might need to disable it on CI servers
    enableUploadProgressBar: false
  },

  mongo: {
    oplog: true,
    port: 27017,
    version: '3.4.1',
    servers: {
      one: {},
    },
  },
};
