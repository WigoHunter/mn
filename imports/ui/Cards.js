import React from 'react';
import { Link } from 'react-router';
//import ReactDOM from 'react-dom'; Unused import

import {AuthorID} from '../api/getnews.js';
Meteor.subscribe('authors');

const findAuthorNamePic = (authorIDparam, isName) => {
  //console.log(authorIDparam);
  const authorObj = AuthorID.findOne({_id: authorIDparam});

  if(isName)
    return authorObj.name;
  else
    return authorObj.pic;
};

export const mapTypeToTag = type => ({
	interviews: (
    <span className="pic-tag" style={{ backgroundColor: "#FFBC00" }}>專訪</span>
  ),
	musics: (
		<span className="pic-tag" style={{ backgroundColor: "#FFBC00" }}>音樂</span>
	),
	events: (
		<span className="pic-tag" style={{ backgroundColor: "#FF4081" }}>活動</span>
	),
	shows: (
		<span className="pic-tag" style={{ backgroundColor: "#007CB9" }}>演出</span>
	),
	lives: (
		<span className="pic-tag" style={{ backgroundColor: "#FF4081" }}>生活</span>
	),
}[type]);

export const HorizontalCard = ({ data }) => (
  <div className="h-card">
    <div className="pic" style={{ backgroundImage: `url(${data.pic})`, backgroundSize: "cover" }}>
    </div>
    <div className="card-content">
      <h3>{data.title}</h3>
      <h5>{data.timeago}</h5>
      <p>{data.excerpt}</p>
      <h4>查看更多</h4>
    </div>
  </div>
);

export const SquareCard = ({ data }) => (
  <div className="s-card">
    <div className="pic" style={{ backgroundImage: `url(${data.pic})`, backgroundSize: "cover" }}>
      <div className="pic-content">
        <div>
          <h3>{data.title}</h3>
        </div>
      </div>
    </div>
  </div>
);

export const VerticalCard = ({ data }) => (
  <div className="v-card"><Link to={`/newsfeed/${data._id}`}>
    <div className="pic" style={{ backgroundImage: `url(${data.pic})`, backgroundSize: "cover" }}>
      {mapTypeToTag(data.type)}
    </div>
    <div className="card-content">
      <h3>{data.title}</h3>
      <p>{data.excerpt}</p>
      <div className="author">
        <div className="author-pic" style={{ backgroundImage: `url(${findAuthorNamePic(data.author_id, 0)})`, backgroundSize: "cover" }}></div>
        <h4 className="author-name">
          {findAuthorNamePic(data.author_id, 1)}
          <span><time className="timeago" dateTime={data.createdAt.toISOString()}/></span>
        </h4>
      </div>
    </div></Link>
  </div>
);

export const LargeVCard = ({ data }) => (
  <div className="l-v-card"><Link to={`/newsfeed/${data._id}`}>
      <div className="card-content">
        {mapTypeToTag(data.type)}

        <div className = "title-wrapper">
          <h3>{data.title}</h3>
          <p className="time">{data.createdAt.getDate()}-{data.createdAt.getMonth() + 1}-{data.createdAt.getFullYear()}</p>
        </div>
        <div className="pic" style={{ backgroundImage: `url(${data.pic})`, backgroundSize: "cover" }}></div>
        <p className = "excerpt">{data.excerpt}</p>
      </div>
      <div className="card-footer">
        <div className="view-details">
          <p>查看更多 &gt;</p>
        </div>
      </div>
    </Link>
  </div>
);

export const SmallVCard = ({ data }) => (
  <div className="s-v-card"><Link to={`/newsfeed/${data._id}`}>
    <div className="pic" style={{ backgroundImage: `url(${data.pic})`, backgroundSize: "cover" }}>
      {mapTypeToTag(data.type)}
    </div>
    <div className="card-content">
      <h3>{data.title}</h3>
      <p>{data.excerpt}</p>
      <div className="author">
        <div className="author-pic" style={{ backgroundImage: `url(${findAuthorNamePic(data.author_id, 0)})`, backgroundSize: "cover" }}>
        </div>
        <h4 className="author-name">
          {findAuthorNamePic(data.author_id, 1)}
          <span><time className="timeago" dateTime={data.createdAt.toISOString()}/></span>
        </h4>
      </div>
    </div></Link>
  </div>
);

// const CardListContainer = createContainer(({ params }) => {
//   const { typeId } = params;
//   Meteor.subscribe('news', typeId);
//   return {
//     news: News.find({}).fetch(),
//   }
// }, CardList);
