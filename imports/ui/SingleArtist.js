import React from 'react';
import { Link } from 'react-router';
import { createContainer } from 'meteor/react-meteor-data';
import Slider from 'react-slick';

import { AuthorID, News } from '../api/getnews.js';

class SingleArtist extends React.Component {
  render() {
    const { artist, artists, news, loading } = this.props;
    const settings = {
      dots: false,
      infinite: false,
      speed: 500,
      slidesToShow: 3,
      slidesToScroll: 1,
      responsive: [
        { breakpoint: 540, settings: { slidesToShow: 1, arrows: false, swipeToSlide: true } }
      ]
    };

    return (
      <div className="single-artist">
        { loading ? "" :
          <div>
            <div className="head" style={{ backgroundImage: `url(${artist.spic})`, backgroundSize: "cover" }}>
              <div className="overlay"></div>
              <div className="info">
                <h5 className="path">首頁 > 藝人管理 > {artist.name}</h5>
                <div className="profile">
                  <div className="left" style={{ backgroundImage: `url(${artist.spic})`, backgroundSize: "cover" }}></div>
                  <div className="right">
                    <h3>{artist.chiName}</h3>
                    <p>{artist.engName}</p>
                    <div className="social">
                      <a href={artist.facebook} target="_blank"><i className="fa fa-facebook-square" aria-hidden="true"></i></a>
                      {/* <img src="/icons/ig.png" alt="" />
                      <img src="/icons/tt.png" alt="" />*/}
                      <a href={artist.youtube} target="_blank"><i className="fa fa-youtube-square" aria-hidden="true"></i></a>
                      <a href={artist.instagram} target="_blank"><i className="fa fa-instagram" aria-hidden="true"></i></a>
                    </div>
                  </div>
                </div>
                <div className="box">
                  <p>生日: {artist.birthday}</p>
                  <p>身高: {artist.height}</p>
                  <p>星座: {artist.star}</p>
                  <p>語言: {artist.lang}</p>
                  <p>嗜好: {artist.interest}</p>
                  <p>樂器: {artist.instrument}</p>
                  <p>學歷: {artist.school}</p>
                </div>
              </div>
            </div>

            <div className="main">
              <div className="paper">
                <h4>人物介紹</h4>
                <div className="line-thru"></div>
                <div className="cover-pic" style={{ backgroundImage: `url(${artist.cover})`, backgroundSize: "cover" }}></div>
                <p>{artist.description}</p>
                <div className="social">
                  <a href={artist.facebook} target="_blank"><i className="fa fa-facebook-square" aria-hidden="true"></i></a>
                  {/* <img src="/icons/ig.png" alt="" />
                  <img src="/icons/tt.png" alt="" />*/}
                  <a href={artist.youtube} target="_blank"><i className="fa fa-youtube-square" aria-hidden="true"></i></a>
                  <a href={artist.instagram} target="_blank"><i className="fa fa-instagram" aria-hidden="true"></i></a>
                </div>
                <h4>藝人Youtube</h4>
                <div className="line-thru" style={{marginBottom: '20px'}}></div>
                <Slider {...settings}>
                  <iframe width="95%" height="190px" src="https://www.youtube.com/embed/videoseries?list=PLFsdFYRDpGascsvIRIGE-7x1UEfTYdZZF?ecver=1" frameborder="0" allowfullscreen></iframe>
                  <iframe width="95%" height="190px" src="https://www.youtube.com/embed/videoseries?list=PLFsdFYRDpGascsvIRIGE-7x1UEfTYdZZF?ecver=1" frameborder="0" allowfullscreen></iframe>
                  <iframe width="95%" height="190px" src="https://www.youtube.com/embed/videoseries?list=PLFsdFYRDpGascsvIRIGE-7x1UEfTYdZZF?ecver=1" frameborder="0" allowfullscreen></iframe>
                </Slider>
                <div style={{height: '30px'}}></div>

                <h4>相關消息</h4>
                <div className="line-thru"></div>
                <Slider {...settings}>
                  {news.map((single) => (
                    <div className="artist-news">
                      <Link to={`/newsfeed/${single._id}`}>
                        <div className="pic" style={{
                          backgroundImage: `url(${single.pic})`,
                          backgroundSize: 'cover',
                        }}>
                          <div className="overlay"></div>
                          <h3>{single.title}</h3>
                        </div>
                      </Link>
                    </div>
                  ))}
                </Slider>
              </div>
              <div className="outer">
                <h4>其他藝人</h4>
                <div className="line-thru"></div>
                <div className="artists-boxes">
                  {artists.map((artist) => (
                    <Link to={`/artist/${artist._id}`}>
                      <div className="artist-box" style={{ backgroundImage: `url(${artist.pic})`, backgroundSize: "cover" }}>
                        <p>{artist.name}</p>
                      </div>
                    </Link>
                  ))}
                  <div className="more-box"><Link to="/artists">更多藝人</Link></div>
                </div>
              </div>
            </div>
          </div>
        }
      </div>
    );
  }
}

const SingleArtistContainer = createContainer(({ params }) => {
  const { id } = params;

  const sub = Meteor.subscribe('authors');
  const subNews = Meteor.subscribe('authorNews', id);
  const loading = !(sub.ready() && subNews.ready());
  const artist = AuthorID.findOne({ _id: id });
  const artists = AuthorID.find();

  return {
    artist,
    artists,
    news: News.find().fetch(),
    loading
  }
}, SingleArtist);

export default SingleArtistContainer;
