import React from 'react';
import { Link } from 'react-router';
import { createContainer } from 'meteor/react-meteor-data';
import ReactDOM from 'react-dom';

export class ContactForm extends React.Component {
  handleSubmit(event){
    event.preventDefault();
    //alert("Triggered!");

    const name = ReactDOM.findDOMNode(this.refs.name).value;
    const email = ReactDOM.findDOMNode(this.refs.email).value;
    const title = ReactDOM.findDOMNode(this.refs.title).value;
    const content = ReactDOM.findDOMNode(this.refs.content).value;

    const data = {
      to: 'guy@vibecreation.com',
      from: email,
      subject: title,
      text: `${content} \nfrom ${email}\nby ${name}`,
    };

    Meteor.call('send.email', data, (error, isSuccess) => {
    if(error) {
      // handle error
    } else {
      if(isSuccess){
        //Clear the form
        $(ReactDOM.findDOMNode(this.refs.name)).val("");
        $(ReactDOM.findDOMNode(this.refs.email)).val("");
        $(ReactDOM.findDOMNode(this.refs.title)).val("");
        $(ReactDOM.findDOMNode(this.refs.content)).val("");

        var $overlayMessagebox = $(ReactDOM.findDOMNode(this.refs.overlayMessagebox));
        //console.log($overlayMessagebox);
        $overlayMessagebox.fadeIn(300);

        //alert("Test Success.");
        //launch the Success Dialog

      }
    }
  });
    //in db.js
  }

  closeMessagebox(event){
    var $overlayMessagebox = $(ReactDOM.findDOMNode(this.refs.overlayMessagebox));
    $overlayMessagebox.fadeOut(300);
  }

  render() {
  return (
    <div className="contact-form-wrapper">
      <div className = "overlay-messagebox" ref = "overlayMessagebox">
        <div className = "overlay">
        </div>
        <div className = "void-wrapper">
          <div className = "messagebox">
            <h3>訊息已成功發出</h3>
            <p>我們已經收到閣下的訊息，</p>
            <p>我們將會透過電郵聯絡您，謝謝!</p>
            <button onClick={this.closeMessagebox.bind(this)}>確定</button>
          </div>
        </div>
      </div>
      <div className = "banner">
        <div className = "overlay"></div>
        <div className = "little-square">
          <div className = "bg-color"></div>
          <div className = "musicnext-logo">
            <img src="/logo.png"></img>
          </div>
          <div className = "content">
            <h5>MUSICNEXT</h5>
            <p>香港九龍官塘鴻圖道87號越秀大樓2樓</p>
            <p>2/F, Yue Xiu Bldg, 87 Hung To Rd., Kwun Tong, KLN, HK</p>
          </div>
        </div>
      </div>

      <div className = "contact-form">
        <div className = "form-title">
          <h1>聯絡我們</h1>
          <h4>CONTACT US</h4>
        </div>

        <div className = "form-subtitle">
          <p>這裡提供我們的聯絡資料、熱線電話、公司地址的有關資料，</p>
          <p>並備有表格以供閣下賜教垂詢，歡迎與我們聯繫。</p>
        </div>

        <form id="form-element" onSubmit={this.handleSubmit.bind(this)}>
          <input type="text" name="name" ref="name" placeholder="您的姓名" className="input-name"></input>
          <input type="text" name="email" ref="email" placeholder="聯絡電郵" className="input-email"></input>
          <input type="text" name="title" ref="title" placeholder="主題" className="input-title"></input>
          <textarea name="content" ref="content" form="form-element" placeholder="輸入查詢内容..." className="input-content"></textarea>
          <center>
            <input type="submit" value="送出" className="input-button"/>
          </center>
        </form>
      </div>

      <div className="sub-footer" style={{ backgroundImage: "url('/footer.png')", backgroundSize: "cover" }}>
      <div className="footer-wrap">
        <img onClick={() => window.scrollTo(0, 0)} className="go-to-top" src="/to-top.png" />
        <div>
          <h3>聯絡我們</h3>
          <div className="line-thru"></div>
          <h4>musicNext</h4>
          <p>Email: info@wsmmusic.com</p>
          <p>
            香港九龍官塘鴻圖道87號越秀大廈2樓
            <div>2/F, YUE XIU BLDG, 87 HUNG TO RD, KWUN TONG, KLN, HK</div>
            </p>
          <p>TEL: (+852) 3100-1111   FAX: (+852) 3105-3106</p>
          </div>
          <div>
            <h3>追蹤我們</h3>
            <div className="line-thru"></div>
        <div className="social">
          <a href="https://www.facebook.com/musicNEXT.hk" target="_blank"><img src="/icons/fb.png" alt="" /></a>
          <a href="https://www.instagram.com/musicnexthk/" target="_blank"><img src="/icons/ig.png" alt="" /></a>
          <a href="https://www.youtube.com/user/musicnext" target="_blank"><img src="/icons/yt.png" alt="" /></a>
        </div>
        </div>
      </div>
    </div>
  </div>


    );
  }
};

export default ContactForm;

export const ContactUsContainer = createContainer(({ }) => {
}, ContactForm);
