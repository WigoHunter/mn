import React from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';

import { News } from '../api/getnews.js';
import { AuthorID } from '../api/getnews.js';


export class adminNewsList extends React.Component {
  handleSubmit(event) {
    //prevent actual default submission
    event.preventDefault();

    //Find the input
    const title = ReactDOM.findDOMNode(this.refs.title).value;
    const content = ReactDOM.findDOMNode(this.refs.content).value;
    const type = ReactDOM.findDOMNode(this.refs.type).value;
    const artist = ReactDOM.findDOMNode(this.refs.artist).value;
    const thumbnail = ReactDOM.findDOMNode(this.refs.thumbnail).value;

    //Push to DB
    Meteor.call('news.insert', title, content, type, artist, thumbnail);

    //Clear form
    //ReactDOM.findDOMNode(this.refs.input).value = '';
  }

  handleDelete(id) {
    Meteor.call('news.remove', id);
  }

  render() {
    return (
     <div className="admin-cms">

       <form className="admin-new-article" onSubmit={this.handleSubmit.bind(this)}>
         <h2>Admin MiniCMS</h2>
         <input type="text" ref="title" id="input-title" placeholder="Title?" />
         <input type="text" ref="content" id="input-content" placeholder="Content?" />
         <select ref="type" id="select-type">
           <option value="star">星級達人</option>
           <option value="movie">電影</option>
           <option value="music">音樂</option>
           <option value="model">模特兒</option>
           <option value="concert">演唱會</option>
         </select>
         <select ref="artist" id="select-artist">
           {this.props.authorList.map((singleAuthor, i) => (
               <SingleAuthorOption
                   key = {i}
                   Authors = {singleAuthor}
               />
           ))}
         </select>
         <input type="text" ref="thumbnail" id="input-thumbnail" placeholder="Thumbnail URL" />
         <input type="submit" value="Submit" />
       </form>

        <div className="list-container">
          <ul>
            {this.props.adminNews.map((indiNews, i) => (
              <AdminListNewIndividual
                key = {i}
                adminNews = {indiNews}
                onClickDelete = {this.handleDelete}
              />
            ))}
          </ul>
        </div>
     </div>
    );
  }
}

const SingleAuthorOption = ({Authors}) => (
  <option value={Authors._id}>
    {Authors.name}
  </option>
);

const AdminListNewIndividual = ({ adminNews, onClickDelete }) => (
  <li className="">
    <span onClick={() => onClickDelete(adminNews._id)}>
      ＋
    </span>
    <h3>{adminNews.title}</h3>
    <p>{adminNews.excerpt}</p>
  </li>
);


const AdminPageContainer = createContainer(() => {
  Meteor.subscribe('slidenews'); //get all news
  Meteor.subscribe('authors'); //get all authors
  return {
    adminNews: News.find({}).fetch(),
    authorList: AuthorID.find({}).fetch(),
  }
}, adminNewsList);

export default AdminPageContainer;
