import React from 'react';
import { Link } from 'react-router';
import { createContainer } from 'meteor/react-meteor-data';

import { AuthorID } from '../api/getnews.js';

const About = ({}) => (
  <div className="about-wrap">
    <div className="about">
      <h5 className="path"><Link to="/">首頁</Link> > <Link to="/news/all/page/1">關於我們</Link></h5>
      <div className="paper">
        {
          //<div className="cover" style={{ backgroundImage: "url('/about.png')", backgroundSize: "cover" }}>
          //</div>
        }
        {/*
          <div style={{
            width: '100%',
            height: '450px',
            background: "url('https://scontent-sit4-1.xx.fbcdn.net/v/t1.0-9/530626_317792094988242_2056733770_n.jpg?oh=aab9875554975426f0b717f27e22b752&oe=5937D6A7')",
            backgroundSize: 'cover',
            marginBottom: '30px',
          }}>
          </div>
        */}
        <iframe width="100%" height="450px" src="https://www.youtube.com/embed/4b3UYvJjPMk?ecver=1" frameborder="0" allowfullscreen></iframe>
        {/*<iframe width="100%" height="450px" src="https://www.youtube.com/embed/hSwg4fIgthU" frameborder="0" allowfullscreen></iframe>*/}
        <p>musicNEXT 於 2010 年成立，是環星娛樂有限公司旗下的新品牌，致力栽培年輕有活力的實力派新晉歌手，並且旗下藝人多才多藝，包括唱
        作、演繹、跳舞、司儀主持等，各有專長。公司業務範圍包括負責演藝經理人服務，安排旗下藝人歌星參與活動、登台演出及廣告代言人等。此外，
        musicNEXT熱心關注公益活動，形象正面健康，希望為樂壇持續發出正能量，勢成樂壇新力量！</p>

        <h3>追蹤我們</h3>
        <div className="line-thru"></div>

        <div className="social">
          <a href="https://www.youtube.com/user/musicnext" target="_blank"><i className="fa fa-youtube-square" aria-hidden="true"></i></a>
          {/*<i className="fa fa-twitter-square" aria-hidden="true"></i>*/}
          <a href="https://www.facebook.com/musicNEXT.hk/?fref=ts" target="_blank"><i className="fa fa-facebook-square" aria-hidden="true"></i></a>
          <a href="https://www.instagram.com/musicnexthk/" target="_blank"><i className="fa fa-instagram" aria-hidden="true"></i></a>
        </div>

        <h3>總公司</h3>
        <div className="line-thru"></div>

        <div className="company">
          <h4 style={{marginLeft: '0'}}>
            環星音樂國際集團 (WSM Music Group Ltd)
            <div>環星音樂國際集團(WSM Music Group Ltd，簡稱WSM)於1992年成立，業務包括唱片錄音、製作、發行、直銷、音樂版權授權貿易、音樂出版及代理、
            演唱會製作、經理人管理服務，以及引進及推廣歐美經典原唱情歌。而公司旗下的歌手及藝人以資深實力派為主，例如：薛家燕、呂珊、尹光、張偉文、莫旭秋、
            區靄玲、王美蘭等。環星擁有眾多高知名度音樂版權，旗下品牌包括「月昇唱片」、「好萊塢製作」、「創意家族」、「永高創意」、「麗音唱片」、「環星音樂」等，
            可供電影及錄影配樂之用。</div>
          </h4>
        </div>

        {/*
          <h3>樂壇超新星</h3>
          <div className="line-thru"></div>
            {console.log(loading, artists)}
            { loading ? "" :
              <ul className="artists-list">
                {artists.map((artist) => (
                  <Link to={`/artist/${artist._id}`}><li style={{ backgroundImage: `url(${artist.pic})`, backgroundSize: "cover" }}>
                    <h3>{artist.chiName}</h3>
                  </li></Link>
                ))}
              </ul>
            }
        */}
      </div>
    </div>
    <a href="https://www.google.com.hk/maps/place/Yue+Xiu+Industrial+Building,+87+Hung+To+Rd,+Kwun+Tong/@22.3078295,114.2228021,17z/data=!3m1!4b1!4m5!3m4!1s0x34040144efe9d42b:0x761e4126bb8623d6!8m2!3d22.3077862!4d114.224979" target="_blank"><div className="map" style={{ backgroundImage: "url('/image_map.png')", backgroundSize: "cover" }}></div></a>
    <div className="sub-footer" style={{ backgroundImage: "url('/footer.png')", backgroundSize: "cover" }}>
      <div className="footer-wrap">
        <img onClick={() => window.scrollTo(0, 0)} className="go-to-top" src="/to-top.png" />
        <div>
          <h3>聯絡我們</h3>
          <div className="line-thru"></div>
          <h4>musicNext</h4>
          <p>Email: info@wsmmusic.com</p>
          <p>
            香港九龍官塘鴻圖道87號越秀大廈2樓
            <div>2/F, YUE XIU BLDG, 87 HUNG TO RD, KWUN TONG, KLN, HK</div>
          </p>
          <p>TEL: (+852) 3100-1111   FAX: (+852) 3105-3106</p>
        </div>
        <div>
          <h3>追蹤我們</h3>
          <div className="line-thru"></div>
          <div className="social">
            <a href="https://www.facebook.com/musicNEXT.hk" target="_blank"><img src="/icons/fb.png" alt="" /></a>
            <a href="https://www.instagram.com/musicnexthk/" target="_blank"><img src="/icons/ig.png" alt="" /></a>
            <a href="https://www.youtube.com/user/musicnext" target="_blank"><img src="/icons/yt.png" alt="" /></a>
          </div>
        </div>
      </div>
    </div>
  </div>
);

const AboutContainer = createContainer(({}) => {

  return {

  }
}, About);

export default AboutContainer;
