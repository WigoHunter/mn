import React from 'react';
import { Link } from 'react-router';
import { createContainer } from 'meteor/react-meteor-data';
import Slider from 'react-slick';

import { Services } from '../api/getnews.js';

const SingleService = ({ loading, service }) => {
  const settings = {
    infinite: false,
    speed: 700,
    slidesToShow: 5,
    slidesToScroll: 5,
    responsive: [
      { breakpoint: 540, settings: { slidesToShow: 3, slidesToScroll: 3, arrows: true, swipeToSlide: true } },
      { breakpoint: 540, settings: { slidesToShow: 1, slidesToScroll: 1, arrows: false, swipeToSlide: true } }
    ]
  }

  return (
    <div className="single-service-wrap">
      {loading ? "" :
        <div className="single-service">
          <h5 className="path">首頁 > <Link to="/services">專業服務</Link> > {service.title}</h5>
          <div className="paper">
            <h2>{service.title}</h2>
            <div className="cover" style={{ backgroundImage: `url(${service.pic})`, backgroundSize: "cover" }}></div>
            <p>{service.description}</p>
            <h3>租用{service.item}： 每小時HKD${service.rate} (租用時間3小時起)。</h3>
            <div className="line-thru"></div>
            <p>
              以上收費包括提供音響操控技術人員及基本樂隊樂器如下：
              {service.equipments.map((e) => (
                <div>{e};</div>
              ))}
            </p>
            <p>
              備註：
              <div>1)如租用其他指定樂器，務須於租用日期三天前通知，費用另議。</div>
              <div>2)以上所有收費之計算，不足一小時亦作一小時計算。</div>
              <div>凡政府註冊之學校或非牟利團體租用上述服務，可獲特別租金優惠。</div>
            </p>
            <p>Reservation: 9126 5758 (Linus) / 6681 2812 (Ho Kwan Yuen)</p>

            <h3>相關照片</h3>
            <div className="line-thru"></div>
            <div className="environment">
              <Slider {...settings}>
                {service.env.map((e) => (
                  <div style={{}}>
                    <div className="img" style={{backgroundImage: `url(${e})`, backgroundSize: 'cover'}}></div>
                  </div>
                ))}
              </Slider>
            </div>

            <h3>追蹤我們</h3>
            <div className="line-thru"></div>

            <div className="social">
              <a href="https://www.youtube.com/user/musicnext" target="_blank"><i className="fa fa-youtube-square" aria-hidden="true"></i></a>
              {/*<i className="fa fa-twitter-square" aria-hidden="true"></i>*/}
              <a href="https://www.facebook.com/musicNEXT.hk/?fref=ts" target="_blank"><i className="fa fa-facebook-square" aria-hidden="true"></i></a>
              <a href="https://www.instagram.com/musicnexthk/" target="_blank"><i className="fa fa-instagram" aria-hidden="true"></i></a>

            </div>

            <h3>總公司</h3>
            <div className="line-thru"></div>

            <div className="company">
              <h4 style={{marginLeft: '0'}}>
                環星音樂國際集團 (WSM Music Group Ltd)
                <div>環星音樂國際集團(WSM Music Group Ltd，簡稱WSM)於1992年成立，業務包括唱片錄音、製作、發行、直銷、音樂版權授權貿易、音樂出版及代理、
                演唱會製作、經理人管理服務，以及引進及推廣歐美經典原唱情歌。而公司旗下的歌手及藝人以資深實力派為主，例如：薛家燕、呂珊、尹光、張偉文、莫旭秋、
                區靄玲、王美蘭等。環星擁有眾多高知名度音樂版權，旗下品牌包括「月昇唱片」、「好萊塢製作」、「創意家族」、「永高創意」、「麗音唱片」、「環星音樂」等，
                可供電影及錄影配樂之用。</div>
              </h4>
            </div>
          </div>
        </div>
      }
      <a href="https://www.google.com.hk/maps/place/Yue+Xiu+Industrial+Building,+87+Hung+To+Rd,+Kwun+Tong/@22.3078295,114.2228021,17z/data=!3m1!4b1!4m5!3m4!1s0x34040144efe9d42b:0x761e4126bb8623d6!8m2!3d22.3077862!4d114.224979" target="_blank"><div className="map" style={{ backgroundImage: "url('/image_map.png')", backgroundSize: "cover" }}></div></a>
      <div className="sub-footer" style={{ backgroundImage: "url('/footer.png')", backgroundSize: "cover" }}>
        <div className="footer-wrap">
          <img onClick={() => window.scrollTo(0, 0)} className="go-to-top" src="/to-top.png" />
          <div>
            <h3>聯絡我們</h3>
            <div className="line-thru"></div>
            <h4>musicNext</h4>
            <p>Email: info@wsmmusic.com</p>
            <p>
              香港九龍官塘鴻圖道87號越秀大廈2樓
              <div>2/F, YUE XIU BLDG, 87 HUNG TO RD, KWUN TONG, KLN, HK</div>
            </p>
            <p>TEL: (+852) 3100-1111   FAX: (+852) 3105-3106</p>
          </div>
          <div>
            <h3>追蹤我們</h3>
            <div className="line-thru"></div>
            <div className="social">
              <a href="https://www.facebook.com/musicNEXT.hk" target="_blank"><img src="/icons/fb.png" alt="" /></a>
              <a href="https://www.instagram.com/musicnexthk/" target="_blank"><img src="/icons/ig.png" alt="" /></a>
              <a href="https://www.youtube.com/user/musicnext" target="_blank"><img src="/icons/yt.png" alt="" /></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

const SingleServiceContainer = createContainer(({ params }) => {
  const { id } = params;

  const sub = Meteor.subscribe('singleService', id);
  const loading = !sub.ready();
  const service = Services.findOne();

  return {
    loading,
    service
  }
}, SingleService);

export default SingleServiceContainer;
