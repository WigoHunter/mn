import React from 'react';
import { Link } from 'react-router';
import { createContainer } from 'meteor/react-meteor-data';

import { mapTypeToTag } from './Cards.js';

import { News } from '../api/getnews.js';

class SingleNews extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    window.fbAsyncInit = function() {
      FB.init({
        appId      : '559895877540599',
        status     : true,
        xfbml      : true,
        version    : 'v2.5'
      });
    }.bind(this);
  }

  componentDidUpdate() {
    FB.XFBML.parse();
  }

  render() {
    const { loading, news, recent } = this.props;

    return (
      <div className="single-news-wrap">
        {loading ? "" :
          <div className="single-news">
            <h5 className="path"><Link to="/">首頁</Link> > <Link to="/news/all/page/1">最新消息</Link> > {news.title}</h5>
            <div className="paper">
              <div className="dismiss"><Link to="/news/all/page/1">＋</Link></div>
              {mapTypeToTag(news.type)}
              <h2>{news.title}</h2>
              <p className="time">{news.createdAt.getDate()}-{news.createdAt.getMonth() + 1}-{news.createdAt.getFullYear()}</p>
              <div className="cover" style={{ backgroundImage: `url(${news.pic})`, backgroundSize: "cover" }}>
              </div>
              <p className="text" dangerouslySetInnerHTML={{__html: news.story.replace(new RegExp('\n', 'g'), '<br />')}}></p>
              <div className="social">
                <a href="https://www.facebook.com/musicNEXT.hk" target="_blank"><img src="/icons/fb.png" alt="" /></a>
                <a href="https://www.instagram.com/musicnexthk/" target="_blank"><img src="/icons/ig.png" alt="" /></a>
                <a href="https://www.youtube.com/user/musicnext" target="_blank"><img src="/icons/yt.png" alt="" /></a>
              </div>
              <h3>回應</h3>
              <div className="line-thru"></div>
              <div className="fb-comments" data-href={`http://localhost:3000/newsfeed/${news._id}`} data-width="100%" data-numposts="5"></div>
              <h3>熱門新聞</h3>
              <div className="line-thru"></div>
              <ul>
                {recent.map((news) => (
                  <Link to={`/newsfeed/${news._id}`}><li className="recent" style={{ backgroundImage: `url(${news.pic})`, backgroundSize: "cover" }}>
                    <h4>{news.title}</h4>
                  </li></Link>
                ))}
              </ul>
            </div>
          </div>
        }
      </div>
    );
  }
}

const SingleNewsContainer = createContainer(({ params }) => {
  const { id } = params;

  const sub = Meteor.subscribe('newsfeed', id);
  const sub2 = Meteor.subscribe('recentnews');
  const loading = !(sub.ready() && sub2.ready());
  const news = News.findOne({ _id: id });
  const recent = News.find({}, { sort: {createdAt: -1}, limit: 4 }).fetch();

  return {
    loading,
    news,
    recent,
  }
}, SingleNews);

export default SingleNewsContainer;
