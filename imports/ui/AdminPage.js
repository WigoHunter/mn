import React from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import { Link, router } from 'react-router';

import { News } from '../api/getnews.js';
import { AuthorID, HomeData, Images } from '../api/getnews.js';

const findAuthorName = (authorID) => {
  //console.log(authorIDparam);
  const param = `${authorID}`
  const authorObj = AuthorID.findOne({_id: param});
  if(authorObj) return authorObj.name;
};

export const mapTypeEngToTypeChi = type => ({
	musics: ('音樂'),
  interviews: ('專訪'),
  events: ('活動'),
  lives: ('生活'),
}[type]);

class TextInput extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: this.props.text,
    }
  }

  handleChange(e) {
    this.setState({value: e.target.value});
  }

  render() {
    return (
      <input value={this.state.value} type="text" onChange={this.handleChange.bind(this)} />
    );
  }
}

class SelectInput extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: this.props.choice,
    }
  }

  handleChange(e) {
    this.setState({value: e.target.value});
  }

  render() {
    return (
      <select value={this.state.value} onChange={this.handleChange.bind(this)}>
        {this.props.options.map((opt) => (
          <option value={opt}>{findAuthorName(opt)}</option>
        ))}
      </select>
    );
  }
}

class SelectTypeInput extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: this.props.choice,
    }
  }

  handleChange(e) {
    this.setState({value: e.target.value});
  }

  render() {
    return (
      <select value={this.state.value} onChange={this.handleChange.bind(this)}>
        {this.props.options.map((opt) => (
          <option value={opt}>{mapTypeEngToTypeChi(opt)}</option>
        ))}
      </select>
    );
  }
}

class TextareaInput extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: this.props.text,
    }
  }

  handleChange(e) {
    this.setState({value: e.target.value});
  }

  render() {
    return (
      <textarea value={this.state.value} onChange={this.handleChange.bind(this)} />
    );
  }
}

export class CMSAddArtist extends React.Component {
  constructor(props) {
    super(props);

    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(e) {
    e.preventDefault();

    let picPath = '', spicPath = '', cover = '';

    const upload = Images.insert({
      file: ReactDOM.findDOMNode(this.refs.pic).files[0],
    }, false);

    upload.on('end', (err, fileObj) => {
      if(err) {
        alert(`Error during upload: ${err}`);
      } else {
        picPath = `/cdn/storage/Images/${fileObj._id}/original/${fileObj._id}.${fileObj.extension}`;

        const uploadSpic = Images.insert({
          file: ReactDOM.findDOMNode(this.refs.spic).files[0],
        }, false);

        uploadSpic.on('end', (err, fileObj2) => {
          spicPath = `/cdn/storage/Images/${fileObj2._id}/original/${fileObj2._id}.${fileObj2.extension}`

          const uploadCover = Images.insert({
            file: ReactDOM.findDOMNode(this.refs.cover).files[0],
          }, false);

          uploadCover.on('end', (err, fileObj3) => {
            cover = `/cdn/storage/Images/${fileObj3._id}/original/${fileObj3._id}.${fileObj3.extension}`;

            Meteor.call('add.artist', {
              '_id': `${this.props.artists.length + 1}`,
              'name': ReactDOM.findDOMNode(this.refs.name).value,
              'chiName': ReactDOM.findDOMNode(this.refs.chiName).value,
              'engName': ReactDOM.findDOMNode(this.refs.engName).value,
              'birthday': ReactDOM.findDOMNode(this.refs.birthday).value,
              'height': ReactDOM.findDOMNode(this.refs.height).value,
              'star': ReactDOM.findDOMNode(this.refs.star).value,
              'lang': ReactDOM.findDOMNode(this.refs.lang).value,
              'interest': ReactDOM.findDOMNode(this.refs.interest).value,
              'instrument': ReactDOM.findDOMNode(this.refs.instrument).value,
              'school': ReactDOM.findDOMNode(this.refs.school).value,
              'pic': picPath,
              'spic': spicPath,
              'cover': cover,
              'description': ReactDOM.findDOMNode(this.refs.description).value,
              'story': ReactDOM.findDOMNode(this.refs.story).value,
              'facebook': ReactDOM.findDOMNode(this.refs.facebook).value,
              'youtube': ReactDOM.findDOMNode(this.refs.youtube).value,
            });

            alert("Uploaded successfully!");

            window.location.reload();
          });

          uploadCover.start();
        });

        uploadSpic.start();
      }
    });

    upload.start();
  }

  render() {
    return (
      <div>
        <h2 className="title">新增藝人</h2>
        <ul className="artists">
            <li>
              <form onSubmit={(e) => this.onSubmit(e)}>
                <label>
                  全名:
                  <TextInput text="" ref="name" />
                </label>
                <label>
                  中文名:
                  <TextInput text="" ref="chiName" />
                </label>
                <label>
                  英文名:
                  <TextInput text="" ref="engName" />
                </label>
                <label>
                  生日:
                  <TextInput text="" ref="birthday" />
                </label>
                <label>
                  身高:
                  <TextInput text="" ref="height" />
                </label>
                <label>
                  星座:
                  <TextInput text="" ref="star" />
                </label>
                <label>
                  語言:
                  <TextInput text="" ref="lang" />
                </label>
                <label>
                  興趣:
                  <TextInput text="" ref="interest" />
                </label>
                <label>
                  樂器:
                  <TextInput text="" ref="instrument" />
                </label>
                <label>
                  學校:
                  <TextInput text="" ref="school" />
                </label>
                <label>
                  照片（長）:
                  <input type="file" ref="pic" />
                </label>
                <label>
                  照片（方）:
                  <input type="file" ref="spic" />
                </label>
                <label>
                  Cover Photo:
                  <input type="file" ref="cover" />
                </label>
                <label className="intro">
                  短述:
                  <TextInput text="" ref="description" />
                </label>
                <label className="textarea">
                  故事:
                  <TextareaInput text="" ref="story" />
                </label>
                <label className="intro">
                  Facebook:
                  <TextInput text="" ref="facebook" />
                </label>
                <label className="intro">
                  Youtube:
                  <TextInput text="" ref="youtube" />
                </label>
                <input type="submit" value="新增" />
              </form>
            </li>
        </ul>
      </div>
    );
  }
}

export class CMSArtists extends React.Component {
  constructor(props) {
    super(props);

    this.onSubmitChange = this.onSubmitChange.bind(this);
  }

  onSubmitChange(i, e) {
    e.preventDefault();

    if(ReactDOM.findDOMNode(this.refs[`pic_${i}`]).files[0]) {
      const upload = Images.insert({
        file: ReactDOM.findDOMNode(this.refs[`pic_${i}`]).files[0],
      }, false);

      upload.on('end', (err, fileObj) => {
        if(err) {
          alert(`Error during upload: ${err}`);
        } else {
          Meteor.call('update.artist.pic', i, fileObj._id, fileObj.extension);
        }
      });

      upload.start();
    }

    if(ReactDOM.findDOMNode(this.refs[`spic_${i}`]).files[0]) {
      const upload = Images.insert({
        file: ReactDOM.findDOMNode(this.refs[`spic_${i}`]).files[0],
      }, false);

      upload.on('end', (err, fileObj) => {
        if(err) {
          alert(`Error during upload: ${err}`);
        } else {
          Meteor.call('update.artist.spic', i, fileObj._id, fileObj.extension);
        }
      });

      upload.start();
    }

    if(ReactDOM.findDOMNode(this.refs[`cover_${i}`]).files[0]) {
      const upload = Images.insert({
        file: ReactDOM.findDOMNode(this.refs[`cover_${i}`]).files[0],
      }, false);

      upload.on('end', (err, fileObj) => {
        if(err) {
          alert(`Error during upload: ${err}`);
        } else {
          Meteor.call('update.artist.cover', i, fileObj._id, fileObj.extension);
        }
      });

      upload.start();
    }

    // console.log(ReactDOM.findDOMNode(this.refs[`name_${i}`]).value);
    Meteor.call('updateArtist', i, {
      name: ReactDOM.findDOMNode(this.refs[`name_${i}`]).value,
      chiName: ReactDOM.findDOMNode(this.refs[`chiName_${i}`]).value,
      engName: ReactDOM.findDOMNode(this.refs[`engName_${i}`]).value,
      birthday: ReactDOM.findDOMNode(this.refs[`birthday_${i}`]).value,
      height: ReactDOM.findDOMNode(this.refs[`height_${i}`]).value,
      star: ReactDOM.findDOMNode(this.refs[`star_${i}`]).value,
      lang: ReactDOM.findDOMNode(this.refs[`lang_${i}`]).value,
      interest: ReactDOM.findDOMNode(this.refs[`interest_${i}`]).value,
      instrument: ReactDOM.findDOMNode(this.refs[`instrument_${i}`]).value,
      school: ReactDOM.findDOMNode(this.refs[`school_${i}`]).value,
      description: ReactDOM.findDOMNode(this.refs[`description_${i}`]).value,
      story: ReactDOM.findDOMNode(this.refs[`story_${i}`]).value,
      facebook: ReactDOM.findDOMNode(this.refs[`facebook_${i}`]).value,
      youtube: ReactDOM.findDOMNode(this.refs[`youtube_${i}`]).value,
      instagram: ReactDOM.findDOMNode(this.refs[`instagram_${i}`]).value,
    }, (err, result) => {
      if(result) {
        alert("Updated successfully!")
      }
    });
  }

  hideArtist(i) {
    Meteor.call('hide.artist', i);
  }

  render() {
    const { artists, news, home } = this.props;

    return (
      <div>
        <h2 className="title">藝人管理</h2>
        <ul className="artists">
          {artists.map((artist, i) => (
            <li key={artist._id} className={artist.hide && "hiding"}>
              <span className="hider" onClick={() => this.hideArtist(i)}>隱藏</span>
              <img src={artist.pic} alt="" />
              <form onSubmit={(e) => this.onSubmitChange(i, e)}>
                <label>
                  全名:
                  <TextInput text={artist.name} ref={`name_${i}`} />
                </label>
                <label>
                  中文名:
                  <TextInput text={artist.chiName} ref={`chiName_${i}`} />
                </label>
                <label>
                  英文名:
                  <TextInput text={artist.engName} ref={`engName_${i}`} />
                </label>
                <label>
                  生日:
                  <TextInput text={artist.birthday} ref={`birthday_${i}`} />
                </label>
                <label>
                  身高:
                  <TextInput text={artist.height} ref={`height_${i}`} />
                </label>
                <label>
                  星座:
                  <TextInput text={artist.star} ref={`star_${i}`} />
                </label>
                <label>
                  語言:
                  <TextInput text={artist.lang} ref={`lang_${i}`} />
                </label>
                <label>
                  興趣:
                  <TextInput text={artist.interest} ref={`interest_${i}`} />
                </label>
                <label>
                  樂器:
                  <TextInput text={artist.instrument} ref={`instrument_${i}`} />
                </label>
                <label>
                  學校:
                  <TextInput text={artist.school} ref={`school_${i}`} />
                </label>
                <label>
                  照片（長）:
                  <input type="file" ref={`pic_${i}`} />
                </label>
                <label>
                  照片（方）:
                  <input type="file" ref={`spic_${i}`} />
                  <img className="sq-pic" src={artist.spic} alt="" />
                </label>
                <label>
                  Cover Photo:
                  <input type="file" ref={`cover_${i}`} />
                  <img className="sq-pic" src={artist.cover} alt="" />
                </label>
                <label className="intro">
                  短述:
                  <TextInput text={artist.description} ref={`description_${i}`} />
                </label>
                <label className="textarea">
                  故事:
                  <TextareaInput text={artist.story} ref={`story_${i}`} />
                </label>
                <label className="intro">
                  Facebook:
                  <TextInput text={artist.facebook} ref={`facebook_${i}`} />
                </label>
                <label className="intro">
                  Youtube:
                  <TextInput text={artist.youtube} ref={`youtube_${i}`} />
                </label>

                <label className="intro">
                  Instagram:
                  <TextInput text={artist.Instagram} ref={`instagram_${i}`} />
                </label>

                <input type="submit" value="修改" />
              </form>
            </li>
          ))}
        </ul>
      </div>
    );
  }
}
export class CMSAddNews extends React.Component {
  constructor(props) {
    super(props);

    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(e) {
    e.preventDefault();

    if(ReactDOM.findDOMNode(this.refs.pic).files[0]) {
      const upload = Images.insert({
        file: ReactDOM.findDOMNode(this.refs.pic).files[0],
      }, false);

      let picPath = '';

      upload.on('end', (err, fileObj) => {
        if(err) {
          alert(`Error during upload: ${err}`);
        } else {
          console.log('uploading news image...');
          picPath = `/cdn/storage/Images/${fileObj._id}/original/${fileObj._id}.${fileObj.extension}`;

          Meteor.call('add.news', {
            title: ReactDOM.findDOMNode(this.refs.title).value,
            pic: picPath,
            'author_id': ReactDOM.findDOMNode(this.refs[`author_id`]).value,
            type: ReactDOM.findDOMNode(this.refs.type).value,
            story: ReactDOM.findDOMNode(this.refs.story).value,
            excerpt: ReactDOM.findDOMNode(this.refs.excerpt).value,
            createdAt: new Date(),
          });

          alert("Uploaded successfully!");

          window.location.reload();
        }
      });

      upload.start();
    } else {
      alert("Please at least upload the photo.");
    }
  }

  render() {
    return (
      <div>
        <h2 className="title">新增消息</h2>
        <ul className="news add">
          <li>
            <form onSubmit={(e) => this.onSubmit(e)}>
              <label className="title">
                標題:
                <TextInput text="" ref="title" />
              </label>
              <label>
                圖片:
                <input type="file" ref="pic" />
              </label>
              <label>
                相關藝人:
                <SelectInput
                  choice=""
                  options={[1,2,3,4,5]}
                  ref="author_id"
                />
              </label>
              <label>
                分類:
                <SelectTypeInput
                  choice=""
                  options={['musics','interviews','events','lives']}
                  ref="type"
                />
              </label>
              <label className="title">
                短述:
                <TextInput text="" ref="excerpt" />
              </label>
              <label>
                內文:
                <TextareaInput text="" ref="story" />
              </label>
              <input type='submit' value='新增' />
            </form>
          </li>
        </ul>
      </div>
    );
  }
}

export class CMSNews extends React.Component {
  constructor(props) {
    super(props);

    this.onSubmitChange = this.onSubmitChange.bind(this);
  }

  onSubmitChange(i, e) {
    e.preventDefault();

    if(ReactDOM.findDOMNode(this.refs[`pic_${i}`]).files[0]) {
      const upload = Images.insert({
        file: ReactDOM.findDOMNode(this.refs[`pic_${i}`]).files[0],
      }, false);

      upload.on('end', (err, fileObj) => {
        if(err) {
          alert(`Error during upload: ${err}`);
        } else {
          console.log('uploading news image...');
          Meteor.call('update.news.pic', i, fileObj._id, fileObj.extension);
        }
      });

      upload.start();
    }

    // console.log(ReactDOM.findDOMNode(this.refs[`author_id_${i}`]).value);
    Meteor.call('update.news', i, {
      title: ReactDOM.findDOMNode(this.refs[`title_${i}`]).value,
      'author_id': ReactDOM.findDOMNode(this.refs[`author_id_${i}`]).value,
      type: ReactDOM.findDOMNode(this.refs[`type_${i}`]).value,
      story: ReactDOM.findDOMNode(this.refs[`story_${i}`]).value,
      excerpt: ReactDOM.findDOMNode(this.refs[`excerpt_${i}`]).value,
    }, (err, result) => {
      if(result) {
        alert("Updated successfully!");
      }
    });
  }

  render() {
    const { artists, news, home } = this.props;

    return (
      <div>
        <h2 className="title">最新消息</h2>
        <ul className="news">
          {news.map((single, i) => (
            <li key={single._id}>
              <img src={single.pic} alt="" />
              <form onSubmit={(e) => this.onSubmitChange(single._id, e)}>
                <label className="title">
                  標題:
                  <TextInput text={single.title} ref={`title_${single._id}`} />
                </label>
                <label>
                  圖片:
                  <input type="file" ref={`pic_${single._id}`} />
                </label>
                <label>
                  相關藝人:
                  <SelectInput
                    choice={single.author_id}
                    options={[1,2,3,4,5]}
                    ref={`author_id_${single._id}`}
                  />
                </label>
                <label>
                  分類:
                  <SelectTypeInput
                    choice={single.type}
                    options={['musics','interviews','events','lives']}
                    ref={`type_${single._id}`}
                  />
                </label>
                <label className="title">
                  短述:
                  <TextInput text={single.excerpt} ref={`excerpt_${single._id}`} />
                </label>
                <label>
                  內文:
                  <TextareaInput text={single.story ? single.story : ""} ref={`story_${single._id}`} />
                </label>
                <input type='submit' value='修改' />
              </form>
            </li>
          ))}
        </ul>
      </div>
    );
  }
}

export class CMSHome extends React.Component {
  constructor(props) {
    super(props);

    this.onSubmitChange = this.onSubmitChange.bind(this);
    this.onSubmitAdChange = this.onSubmitAdChange.bind(this);
    this.onSubmitYoutubeChange = this.onSubmitYoutubeChange.bind(this);
  }

  onSubmitYoutubeChange(e) {
    e.preventDefault();

    Meteor.call('update.home.yt', ReactDOM.findDOMNode(this.refs.homeYoutube).value, (err, res) => {
      alert("Updated successfully!");
    });
  }

  onSubmitAdChange(i, e) {
    e.preventDefault();

    if(ReactDOM.findDOMNode(this.refs[`ads${i}`]).files[0]) {
      const upload = Images.insert({
        file: ReactDOM.findDOMNode(this.refs[`ads${i}`]).files[0],
      }, false);

      upload.on('end', (err, fileObj) => {
        if(err) {
          alert(`Error during upload: ${err}`);
        } else {
          console.log('uploading banner image...');
          Meteor.call('update.home.ads', i, fileObj._id, fileObj.extension, (err, res) => {
            if(res)
              alert("Updated successfully!");
          });
        }
      });

      upload.start();
    }
  }

  onSubmitChange(i, e) {
    e.preventDefault();

    if(ReactDOM.findDOMNode(this.refs[`pic${i}`]).files[0]) {
      const upload = Images.insert({
        file: ReactDOM.findDOMNode(this.refs[`pic${i}`]).files[0],
      }, false);

      upload.on('end', (err, fileObj) => {
        if(err) {
          alert(`Error during upload: ${err}`);
        } else {
          console.log('uploading banner image...');
          Meteor.call('update.homecover.pic', i, fileObj._id, fileObj.extension);
        }
      });

      upload.start();
    }

    Meteor.call('update.homeslider', i, {
      "title" : ReactDOM.findDOMNode(this.refs[`title${i}`]).value,
			"time" : ReactDOM.findDOMNode(this.refs[`time${i}`]).value,
			"excerpt" : ReactDOM.findDOMNode(this.refs[`excerpt${i}`]).value,
      "link": ReactDOM.findDOMNode(this.refs[`link${i}`]).value,
    }, (err, res) => {
      if(res) {
        alert("Updated successfully!");
      }
    });

    // console.log(ReactDOM.findDOMNode(this.refs[`author_id_${i}`]).value);
    // Meteor.call('update.news', i, {
    //   title: ReactDOM.findDOMNode(this.refs[`title_${i}`]).value,
    //   'author_id': ReactDOM.findDOMNode(this.refs[`author_id_${i}`]).value,
    //   type: ReactDOM.findDOMNode(this.refs[`type_${i}`]).value,
    //   story: ReactDOM.findDOMNode(this.refs[`story_${i}`]).value,
    //   excerpt: ReactDOM.findDOMNode(this.refs[`excerpt_${i}`]).value,
    // }, (err, result) => {
    //   if(result) {
    //     alert("Updated successfully!");
    //   }
    // });
  }

  render() {
    const { artists, news, home } = this.props;

    return (
      <div>
        <h2 className="title">首頁管理</h2>
        <ul className="news">
          <li>
            <img src={home.sliders[0].pic} alt="" />
            <form onSubmit={(e) => this.onSubmitChange(0, e)}>
              <label>
                標題:
                <TextInput text={home.sliders[0].title} ref="title0" />
              </label>

              <label>
                時間:
                <TextInput text={home.sliders[0].time} ref="time0" />
              </label>

              <label className="title">
                 連結:
                 <TextInput text={home.sliders[0].link} ref="link0" />
              </label>

              <label className="title">
                短敘:
                <TextInput text={home.sliders[0].excerpt} ref="excerpt0" />
              </label>

              <label>
                圖片:
                <input type="file" ref="pic0" />
              </label>

              <input type='submit' value='修改' />
            </form>
          </li>

          <li>
            <img src={home.sliders[1].pic} alt="" />
            <form onSubmit={(e) => this.onSubmitChange(1, e)}>
              <label>
                標題:
                <TextInput text={home.sliders[1].title} ref="title1" />
              </label>

              <label>
                時間:
                <TextInput text={home.sliders[1].time} ref="time1" />
              </label>

              <label className="title">
                連結:
                <TextInput text={home.sliders[1].link} ref="link1" />
              </label>

              <label className="title">
                短敘:
                <TextInput text={home.sliders[1].excerpt} ref="excerpt1" />
              </label>

              <label>
                圖片:
                <input type="file" ref="pic1" />
              </label>

              <input type='submit' value='修改' />
            </form>
          </li>

          <li>
            <img src={home.sliders[2].pic} alt="" />
            <form onSubmit={(e) => this.onSubmitChange(2, e)}>
              <label>
                標題:
                <TextInput text={home.sliders[2].title} ref="title2" />
              </label>

              <label>
                時間:
                <TextInput text={home.sliders[2].time} ref="time2" />
              </label>

              <label className="title">
                連結:
                <TextInput text={home.sliders[2].link} ref="link2" />
              </label>

              <label className="title">
                短敘:
                <TextInput text={home.sliders[2].excerpt} ref="excerpt2" />
              </label>

              <label>
                圖片:
                <input type="file" ref="pic2" />
              </label>
              <input type='submit' value='修改' />
            </form>
          </li>

          <li>
            <img style={{width: '495.5px'}} src={home.ads[0]} alt="" />
            <form onSubmit={(e) => this.onSubmitAdChange(0, e)}>
              <label>
                圖片:
                <input type="file" ref="ads0" />
              </label>
              <input type='submit' value='修改' />
            </form>
          </li>

          <li>
            <img style={{width: '495.5px'}} src={home.ads[1]} alt="" />
            <form onSubmit={(e) => this.onSubmitAdChange(1, e)}>
              <label>
                圖片:
                <input type="file" ref="ads1" />
              </label>
              <input type='submit' value='修改' />
            </form>
          </li>

          <li>
            <img style={{width: '495.5px'}} src={home.ads[2]} alt="" />
            <form onSubmit={(e) => this.onSubmitAdChange(2, e)}>
              <label>
                圖片:
                <input type="file" ref="ads2" />
              </label>
              <input type='submit' value='修改' />
            </form>
          </li>

          <li className="just-youtube">
            <form onSubmit={(e) => this.onSubmitYoutubeChange(e)}>
              <label className="title">
                Youtube:
                <TextInput text={home.youtube} ref="homeYoutube" />
              </label>
              <input type='submit' value='修改' />
            </form>
          </li>
        </ul>
      </div>
    );
  }
}


class Admin extends React.Component {
  constructor(props) {
    super(props);

    this.login = this.login.bind(this);
  }

  login(e) {
    e.preventDefault();

    Meteor.loginWithPassword(ReactDOM.findDOMNode(this.refs.username).value, ReactDOM.findDOMNode(this.refs.password).value, (err) => {
      if(err) {
        alert("Login failed");
      }
    });
  }

  render() {
    const { loading, artists, news, user, home, children } = this.props;

    return (
      <div className="admin">
        <div className={`menu ${user ? "" : "disable"}`}>
          <img src="/logo.png" alt="" />
          <ul>
            <li className={location.pathname === '/admin/cms/home' ? "active" : ""}><Link to="/admin/cms/home">首頁管理</Link></li>
            <li className={location.pathname === '/admin/cms/artists' ? "active" : ""}><Link to="/admin/cms/artists">藝人管理</Link></li>
            <li className={location.pathname === '/admin/cms/news' ? "active" : ""}><Link to="/admin/cms/news">最新消息</Link></li>
            <li className={location.pathname === '/admin/cms/artists/new' ? "active" : ""}><Link to="/admin/cms/artists/new">新增藝人</Link></li>
            <li className={location.pathname === '/admin/cms/news/new' ? "active" : ""}><Link to="/admin/cms/news/new">新增消息</Link></li>
          </ul>
        </div>
        {user && !loading ?
          <div className="cms">
            {location.pathname === '/admin' ? "" :
              React.cloneElement(children, {
                artists,
                news,
                home
              })
            }
          </div>
        :
          <div className="loginpanel">
  			    <div className="logincard">
              <form id="loginform" onSubmit={this.login}>
                <img src="/logo.png" alt="LOGO" style={{height: '80px'}} />
                <div style={{fontSize:"18px", padding:"15px 0", color:"#EB5E28"}}>後台管理系統</div>
                <input type="text" ref="username" name="account" placeholder="用戶名" />
                <input type="password" ref="password" name="passwd" placeholder="密碼" />
                <input type="submit" name="submit" value="登入" style={{textAlign: "center", width:"100px", height:"40px", backgroundColor:"#00BBFF", marginTop: "10px", color:"white", border:"1px solid #00BBFF"}} />
              </form>
            </div>
          </div>
        }
      </div>
    );
  }
}

const AdminPageContainer = createContainer(() => {
  const subNews = Meteor.subscribe('allnews');
  const subArtists = Meteor.subscribe('authors');
  const subImages = Meteor.subscribe('files.images.all');
  const subHome = Meteor.subscribe('home');
  const loading = !((subNews.ready() && subArtists.ready()) && (subImages.ready() && subHome.ready()));

  return {
    loading,
    artists: AuthorID.find().fetch(),
    news: News.find().fetch(),
    user: Meteor.userId(),
    home: HomeData.findOne(),
  }
}, Admin);

export default AdminPageContainer;
