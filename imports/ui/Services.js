import React from 'react';
import { Link } from 'react-router';
import { createContainer } from 'meteor/react-meteor-data';

import { Services } from '../api/getnews.js';

const ServicesPage = ({ loading, services }) => (
  <div className="service-wrap">
    <div className="services">
      <h5 className="path">首頁 > 專業服務</h5>
      <h2 className="title">
        專業服務
        <div className="trans">SERVICES</div>
      </h2>
      { loading ? "" :
        <div className="service-list">
          {services.map((service) => (
            <Link to={`/service/${service._id}`}><div className="single-service" style={{ backgroundImage: `url(${service.pic})`, backgroundSize: "cover" }}>
              <div className="box">
                <h3>{service.item}出租</h3>
                <p>查詢電話: 3100 11</p>
                <h4>
                  租用綵排室: 每小時HK${service.rate}
                  <div>(租用時間3小時起)</div>
                  <div>{service.description.substring(0, 39)}</div>
                </h4>
              </div>
            </div></Link>
          ))}
        </div>
      }
    </div>
    <a href="https://www.google.com.hk/maps/place/Yue+Xiu+Industrial+Building,+87+Hung+To+Rd,+Kwun+Tong/@22.3078295,114.2228021,17z/data=!3m1!4b1!4m5!3m4!1s0x34040144efe9d42b:0x761e4126bb8623d6!8m2!3d22.3077862!4d114.224979" target="_blank"><div className="map" style={{ backgroundImage: "url('/image_map.png')", backgroundSize: "cover" }}></div></a>
    <div className="sub-footer" style={{ backgroundImage: "url('/footer.png')", backgroundSize: "cover" }}>
      <div className="footer-wrap">
        <img onClick={() => window.scrollTo(0, 0)} className="go-to-top" src="/to-top.png" />
        <div>
          <h3>聯絡我們</h3>
          <div className="line-thru"></div>
          <h4>musicNext</h4>
          <p>Email: info@wsmmusic.com</p>
          <p>
            香港九龍官塘鴻圖道87號越秀大廈2樓
            <div>2/F, YUE XIU BLDG, 87 HUNG TO RD, KWUN TONG, KLN, HK</div>
          </p>
          <p>TEL: (+852) 3100-1111   FAX: (+852) 3105-3106</p>
        </div>
        <div>
          <h3>追蹤我們</h3>
          <div className="line-thru"></div>
          <div className="social">
            <a href="https://www.facebook.com/musicNEXT.hk" target="_blank"><img src="/icons/fb.png" alt="" /></a>
            <a href="https://www.instagram.com/musicnexthk/" target="_blank"><img src="/icons/ig.png" alt="" /></a>
            <a href="https://www.youtube.com/user/musicnext" target="_blank"><img src="/icons/yt.png" alt="" /></a>
          </div>
        </div>
      </div>
    </div>
  </div>
);

const ServiceContainer = createContainer(() => {
  const sub = Meteor.subscribe('services');
  const loading = !sub.ready();
  const services = Services.find().fetch();

  return {
    loading,
    services
  }
}, ServicesPage);

export default ServiceContainer;

export const Links = () => (
  <div className="service-wrap">
    <div className="services">
      <h5 className="path">首頁 > 友好連結</h5>

      <h2 className="title">
        友好連結
        <div className="trans">LINKS</div>
      </h2>
      <ul className="link-list">
        <li>
          <img src="/links/itune-banner.png" alt="" />
          <p>ITUNES</p>
        </li>
        <li>
          <img src="/links/wsm-fb-banner.png" alt="" />
          <p>環星 官方Facebook</p>
        </li>
        <li>
          <img src="/links/wsm-yt-banner.png" alt="" />
          <p>環星 官方Youtube</p>
        </li>
        <li>
          <img src="/links/mn-banner.png" alt="" />
          <p>MUSIC NEXT</p>
        </li>
        <li>
          <img src="/links/mn-yt-banner.png" alt="" />
          <p>MUSIC NEXT 官方Youtube</p>
        </li>
        <li>
          <img src="/links/mn-fb-banner.png" alt="" />
          <p>MUSIC NEXT 官方Facebook</p>
        </li>
        <li>
          <img src="/links/moov-banner.png" alt="" />
          <p>MOOV</p>
        </li>
      </ul>
    </div>
    <a href="https://www.google.com.hk/maps/place/Yue+Xiu+Industrial+Building,+87+Hung+To+Rd,+Kwun+Tong/@22.3078295,114.2228021,17z/data=!3m1!4b1!4m5!3m4!1s0x34040144efe9d42b:0x761e4126bb8623d6!8m2!3d22.3077862!4d114.224979" target="_blank"><div className="map" style={{ backgroundImage: "url('/image_map.png')", backgroundSize: "cover" }}></div></a>
    <div className="sub-footer" style={{ backgroundImage: "url('/footer.png')", backgroundSize: "cover" }}>
      <div className="footer-wrap">
        <img onClick={() => window.scrollTo(0, 0)} className="go-to-top" src="/to-top.png" />
        <div>
          <h3>聯絡我們</h3>
          <div className="line-thru"></div>
          <h4>musicNext</h4>
          <p>Email: info@wsmmusic.com</p>
          <p>
            香港九龍官塘鴻圖道87號越秀大廈2樓
            <div>2/F, YUE XIU BLDG, 87 HUNG TO RD, KWUN TONG, KLN, HK</div>
          </p>
          <p>TEL: (+852) 3100-1111   FAX: (+852) 3105-3106</p>
        </div>
        <div>
          <h3>追蹤我們</h3>
          <div className="line-thru"></div>
          <div className="social">
            <a href="https://www.facebook.com/musicNEXT.hk" target="_blank"><img src="/icons/fb.png" alt="" /></a>
            <a href="https://www.instagram.com/musicnexthk/" target="_blank"><img src="/icons/ig.png" alt="" /></a>
            <a href="https://www.youtube.com/user/musicnext" target="_blank"><img src="/icons/yt.png" alt="" /></a>
          </div>
        </div>
      </div>
    </div>
  </div>
)
