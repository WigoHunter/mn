import React from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import { Template } from 'meteor/templating';
//import './body.html';

import { News } from '../api/getnews.js';

function InjectJS(){
  $.getScript("/js/playSlides.js");
}

function returnNewsType(type){
  switch (type){
    case 'star':
      return(<div className='w3-container w3-amber w3-text-white slide-type-desc'>星級達人</div>);
      break;
    case 'movie':
      return(<div className='w3-container w3-blue slide-type-desc'>電影</div>);
      //color is temporary, not specified in design draft
      break;
    case 'music':
      return(<div className='w3-container w3-purple slide-type-desc'>音樂</div>);
      break;
    case 'model':
      return(<div className='w3-container w3-cyan w3-text-white slide-type-desc'>模特兒</div>);
      break;
    case 'concert':
      return(<div className='w3-container w3-pink slide-type-desc'>演唱會</div>);
      break;
  }
}

export class SlideShow extends React.Component {
  render() {
    return (
      <div className="slideshow-with-indicator">
        <div className="w3-card-2 w3-hover-shadow slideshow" id="SlideList">
          {this.props.slidenews.map((individualNews, i) => (
            <Slide
              key={i}
              slidenews={individualNews}
            />
          ))}
        </div>
        <div className="SlideIndicator">
          {this.props.slidenews.map((individualNews, i) => (
            <Dot
              key={i}
              />
          ))}
        </div>
      </div>
    );
  }
}


const Slide = ({ slidenews }) => (
  <div className="individual-slide fade">
    <img src={slidenews.thumbnail}></img>
    <div>{returnNewsType(slidenews.type)}</div>
    <div className="slide-text">
      <div className="slide-title">{slidenews.title}</div>
      <div className="slide-content">{slidenews.content}</div>
    </div>
  </div>
);

const Dot = ({}) => (
  <span className="dot"></span>
)

const SlideShowContainer = createContainer(({ }) => {
  Meteor.subscribe('slidenews', function(){ InjectJS() });
  return {
    slidenews: News.find({}).fetch(),
  }
}, SlideShow);

export default SlideShowContainer;
