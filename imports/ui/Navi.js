import React from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router';
import { Navbar, Nav, NavItem } from 'react-bootstrap';
// import './Navi.css';

class Navi extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      path: location.pathname,
    }
  }

  onClick() {
    this.setState({
      path: location.pathname
    });
  }

  render() {
    return (
      <div className="nav wrap">
        <div className="container full">
          <div className="logo">
            <Link to="/"><img src="/logo.png" alt="" /></Link>
          </div>

          <ul className="menu-items">
            <Link to="/"><li className="nav-item active" id="nav-main">首頁</li></Link>
            <Link to="/artists"><li className="nav-item" id="nav-artist">藝人管理</li></Link>
            <Link to="/products"><li className="nav-item" id="nav-product">產品</li></Link>
            <Link to="/news/all/page/1"><li className="nav-item" id="nav-movie">最新消息</li></Link>
            <Link to="/services"><li className="nav-item" id="nav-model">專業服務</li></Link>
            <Link to="/links"><li className="nav-item" id="nav-model">友好連結</li></Link>
            <Link to="/about"><li className="nav-item" id="nav-concert">關於我們</li></Link>
            <Link to="/contact"><li className="nav-item" id="nav-concert">聯絡我們</li></Link>
          </ul>

          <ul className="social-media">
            <li>
              {/*<i className="fa fa-instagram" aria-hidden="true"></i>*/}
              <a href="https://www.youtube.com/user/musicnext" target="_blank"><i className="fa fa-youtube-square" aria-hidden="true"></i></a>
              {/*<i className="fa fa-twitter-square" aria-hidden="true"></i>*/}
              <a href="https://www.facebook.com/musicNEXT.hk/?fref=ts" target="_blank"><i className="fa fa-facebook-square" aria-hidden="true"></i></a>
              <a href="https://www.instagram.com/musicnexthk/" target="_blank"><i className="fa fa-instagram" aria-hidden="true"></i></a>
            </li>
          </ul>
        </div>

        <div className="container responsive">
          <Navbar inverse collapseOnSelect>
            <Navbar.Header>
              <Navbar.Brand>
                <a href="/"><img src="/logo.png" alt="" /></a>
              </Navbar.Brand>
              <Navbar.Toggle />
            </Navbar.Header>
            <Navbar.Collapse>
              <Nav>
                <NavItem eventKey={1}><Link to="/">首頁</Link></NavItem>
                <NavItem eventKey={2}><Link to="/artists">藝人管理</Link></NavItem>
                <NavItem eventKey={3}><Link to="/news/all/page/1">最新消息</Link></NavItem>
                <NavItem eventKey={4}><Link to="/services">專業服務</Link></NavItem>
                <NavItem eventKey={5}><Link to="/about">關於我們</Link></NavItem>
                <NavItem eventKey={6}><Link to="/contact">聯絡我們</Link></NavItem>
              </Nav>
            </Navbar.Collapse>
          </Navbar>
        </div>
      </div>
    );
  }
}

export default Navi;
