import React from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router';

const Footer = () => (
  <div className="footer">
    <div className="copy-right">
      <span className="toHide">建議最佳瀏覽器為 Firefox 17 或以上版本、Chrome 23 或以上版本、Safari 5.1 或以上版本、Internet Explorer 9 或以上版本。</span>
      <span className="toHide"><br/></span>
      Copyright &copy; 2016 WSM Music Group Limited. All rights reserved. Powered by Shopsoln Ltd.
    </div>
  </div>
);

export default Footer;
