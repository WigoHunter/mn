import React from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router';

import Navi from './Navi.js';
import Footer from './Footer.js';

//$.getScript("/js/jquery.timeago.js");
$.getScript("/js/ux.js"); //Inject jQuery UX script into DOM

const App = ({ children }) => (
  <div className="root">
    <Navi />
    <div className="content">
      {children}
    </div>
    <Footer />
  </div>
);

export default App;
