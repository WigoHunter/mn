import React from 'react';
import { Link } from 'react-router';
import { createContainer } from 'meteor/react-meteor-data';

import { AuthorID } from '../api/getnews.js';

class ArtistList extends React.Component {
  render() {
    return (
      <div className="artist-list-wrap">
        <div className="artist-list">
          <h5 className="path"><Link to="/">首頁</Link> > 藝人管理</h5>
          <div className="artists">
            <h3 className="title">
              藝人管理
              <div className="trans">ARTIST MANAGEMENT</div>
            </h3>
            <div className="flexbox">
              {this.props.artists.map((artist) => (
                <Link to={`/artist/${artist._id}`}>
                  <div className="artist-card" style={{ backgroundImage: `url(${artist.pic})`, backgroundSize: "cover" }}>
                    <div>
                      <h3>{artist.name}</h3>
                      <p>{artist.description}</p>
                    </div>
                  </div>
                </Link>
              ))}
              <div className="artist-card dummy"></div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const ArtistContainer = createContainer(() => {
  Meteor.subscribe('authors');

  return {
    artists: AuthorID.find().fetch(),
  }
}, ArtistList);

export default ArtistContainer;
