import React from 'react';
import { Link } from 'react-router';
import { createContainer } from 'meteor/react-meteor-data';
//import ReactDOM from 'react-dom'; unused import

import { VerticalCard, LargeVCard, SmallVCard } from './Cards.js';

import { News } from '../api/getnews.js';

const NewsList = ({ data, limit }) => {
  let spaces = [];
  let count = 0;
  let leng = data.length;
  console.log( count + leng );
  console.log( leng );

  while (((count + leng)!= 2) && (((count + leng)!= 5) && ((count + leng - 5) % 4 != 0))) {
    spaces.push({ number: 1 });
    count++;
  }

  return (
    <div className="newslist">
      <h2 className="title">
        最新消息
        <div className="trans">NEWS</div>
      </h2>
      <ul className="filters home">
        <li><Link to="/news/all/page/1">全部</Link></li>
        <li><Link to="/news/musics/page/1">音樂</Link></li>
        <li><Link to="/news/interviews/page/1">專訪</Link></li>
        <li><Link to="/news/events/page/1">活動</Link></li>
        <li><Link to="/news/lives/page/1">生活</Link></li>
      </ul>
      <div className="list">
        {data.map((news, i) => {
          if ( i < limit )
            return (
              i < 2 ? <LargeVCard key = {i} data={news} /> :
              (i < 5 ? <VerticalCard key = {i} data={news} /> :
              <SmallVCard key = {i} data={news} />)
            );
        })}
        {spaces.map((space) => leng < 3 ? <div style={{width: "30%"}}></div> : <div style={{width: "23%"}}></div>)}
      </div>
    </div>
  );
};

export default NewsList;

const NewsPage = ({ data, pages, filter, limit, loading }) => {
  let spaces = [];
  let count = 0;
  let leng = data.length;
  let pagesleng = [];

  while (((count + leng)!= 2) && ((count + leng)!= 5) && ((count + leng - 5) % 4 != 0)) {
    spaces.push({ number: 1});
    count++;
  }

  for(let i = 1; i <= pages; i++) {
    pagesleng.push(i);
  }

  return (
    <div className="news-outer">
      <div className="news-page">
        <h5 className="path">首頁 > 最新消息</h5>
        <div className="newslist">
          <h2 className="title">
            最新消息
            <div className="trans">NEWS</div>
          </h2>
          <ul className="filters">
            <li className={location.pathname.split('/')[2] === 'all' ? "active" : ""}><Link to="/news/all/page/1">全部</Link></li>
            <li className={location.pathname.split('/')[2] === 'musics' ? "active" : ""}><Link to="/news/musics/page/1">音樂</Link></li>
            <li className={location.pathname.split('/')[2] === 'interviews' ? "active" : ""}><Link to="/news/interviews/page/1">專訪</Link></li>
            <li className={location.pathname.split('/')[2] === 'events' ? "active" : ""}><Link to="/news/events/page/1">活動</Link></li>
            <li className={location.pathname.split('/')[2] === 'lives' ? "active" : ""}><Link to="/news/lives/page/1">生活</Link></li>
          </ul>

          { loading ? "" :
            <div className="list">
              {data.map((news, i) => {
                if ( i < limit )
                  return (
                    i < 2 ? <LargeVCard key = {i} data={news} /> :
                    ( i < 5 ? <VerticalCard key = {i} data={news} /> :
                      <SmallVCard key = {i} data={news} /> )
                    );
                  })
              }
              { spaces.map((space) => leng < 2 ? <div style={{width: "48%"}}></div> :
                ( leng < 5 ? <div style={{width: "30%"}}></div> :
                  <div style={{width: "23%"}}></div>
                ))
              }
            </div>
          }

          <ul className="page-num">
            {pagesleng.map((page) => (
              <li className={page == parseInt(location.pathname.split("/")[4]) ? "selected" : ""}>
                <Link to={`/news/${filter}/page/${page}`}>{page}</Link>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  )
};

export const NewsContainer = createContainer(({ params }) => {
  const { filter, page } = params;

  let sub;

  if(filter) {
    sub = Meteor.subscribe('news', filter, parseInt(page));
  } else {
    sub = Meteor.subscribe('news', 'all', parseInt(page));
  }

  const loading = !sub.ready;

  const pageNum = Math.ceil(News.find().fetch().length / 13);

  return {
    data: News.find({}, {sort: {createdAt: -1}, skip: (page - 1) * 13, limit: page * 13}).fetch().sort((a, b) => (b.createdAt - a.createdAt)),
    pages: pageNum,
    filter,
    limit: 13,
    loading,
  }
}, NewsPage);
