import React from 'react';
import { Link } from 'react-router';
import { createContainer } from 'meteor/react-meteor-data';

import { AuthorID } from '../api/getnews.js';

class Products extends React.Component {
  render() {
    return (
      <div className="artist-list-wrap">
        <div className="artist-list">
          <h5 className="path"><Link to="/">首頁</Link> > 產品</h5>
          <div className="artists">
            <h3 className="title">
              產品
              <div className="trans">PRODUCTS</div>
            </h3>
            <div className="flexbox">
              {this.props.artists.map((artist) => (
                <Link to={`http://139.162.13.104/search/${artist.chiName}`}>
                  <div className="artist-card product" style={{ backgroundImage: `url(${artist.pic})`, backgroundSize: "cover" }}>
                    <div>
                      <h3>{artist.name}</h3>
                    </div>
                  </div>
                </Link>
              ))}
              <div className="artist-card dummy"></div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const ProductContainer = createContainer(() => {
  Meteor.subscribe('authors');

  return {
    artists: AuthorID.find().fetch(),
  }
}, Products);

export default ProductContainer;
