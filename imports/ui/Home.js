import React from 'react';
import { Link } from 'react-router';
import { createContainer } from 'meteor/react-meteor-data';
import Slider from 'react-slick';

import { News, AuthorID, Services, HomeData } from '../api/getnews.js';

import { HorizontalCard, SquareCard } from './Cards.js';
import NewsList from './News.js';


class Home extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      limit: 13,
      await: false,
    };
  }

  clickSeeMore() {
    this.setState({ await: true });
    setTimeout(() => {
      this.setState({ await: false, limit: this.state.limit + 8, });
    }, 500);
  }

  render() {

    const { data, artists, services, home, loading } = this.props;

    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 10000,
    };
    const settingsAd = {
      dots: false,
      className: "ads",
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
    };

    console.log(home);

    return (
      <div>
        {!loading &&
        <div>
          <div className="header" id="header">
            <Slider {...settings}>
              <div className="cover" id="cover-photo" style={{ backgroundImage: `url(${home.sliders[0].pic})`, backgroundSize: "cover"}}>
                <div className="cover-overlay"></div>
                <div className="cover-content">
                  <h3>{home.sliders[0].title}</h3>
                  <h5>{home.sliders[0].time}</h5>
                  <p>{home.sliders[0].excerpt}</p>
                  <h4><Link to={home.sliders[0].link}>查看更多</Link></h4>
                </div>
              </div>

              <div className="cover" id="cover-photo" style={{ backgroundImage: `url(${home.sliders[1].pic})`, backgroundSize: "cover"}}>
                <div className="cover-overlay"></div>
                <div className="cover-content">
                  <h3>{home.sliders[1].title}</h3>
                  <h5>{home.sliders[1].time}</h5>
                  <p>{home.sliders[1].excerpt}</p>
                  <h4><Link to={home.sliders[1].link}>查看更多</Link></h4>
                </div>
              </div>

              <div className="cover" id="cover-photo" style={{ backgroundImage: `url(${home.sliders[2].pic})`, backgroundSize: "cover"}}>
                <div className="cover-overlay"></div>
                <div className="cover-content">
                  <h3>{home.sliders[2].title}</h3>
                  <h5>{home.sliders[2].title}</h5>
                  <p>{home.sliders[2].excerpt}</p>
                  <h4><Link to={home.sliders[2].link}>查看更多</Link></h4>
                </div>
              </div>
            </Slider>
            <div className="news">
              <Slider {...settingsAd}>
                <div className="left-subhead" style={{ backgroundImage: `url(${home.ads[0]})`, backgroundSize: "cover" }}></div>
                <div className="left-subhead" style={{ backgroundImage: `url(${home.ads[1]})`, backgroundSize: "cover" }}></div>
                <div className="left-subhead" style={{ backgroundImage: `url(${home.ads[2]})`, backgroundSize: "cover" }}></div>
              </Slider>
              <span className="iframe"><iframe width="100%" height="220" src={home.youtube} frameborder="0" allowfullscreen></iframe></span>
            </div>
          </div>
          <div className="artists">
            <h2 className="title">
              藝人管理
              <div className="trans">ARTIST MANAGEMENT</div>
            </h2>
            <div className="artists-boxes">
              {artists.map((artist) => (
                <Link to={`/artist/${artist._id}`}>
                  <div className="artist-box" style={{ backgroundImage: `url(${artist.pic})`, backgroundSize: "cover" }}>
                    <p>{artist.name}</p>
                  </div>
                </Link>
              ))}
              <div className="more-box"><Link to="/artists">更多藝人</Link></div>
            </div>
            <Link to="/artists"><div className="see-more art">查看更多</div></Link>
          </div>
          <div className="news-section">
            {loading ? "" :
              <NewsList data = {data} limit = {this.state.limit} />
            }
            <Link to="/news/all/page/1"><div className="see-more">查看更多</div></Link>

            {/* Services */}
            <h2 className="title service">
              專業服務
              <div className="trans">Services</div>
            </h2>
            <div className="service-boxes">
              {services.map((service) => (
                <div className="service-card">
                  <div className="service-pic" style={{ backgroundImage: `url(${service.pic})`, backgroundSize: "cover"}}></div>
                  <div className="service-right">
                    <div>
                      <h3>{service.item}出租</h3>
                      <p className="number">查詢電話： 3100 111</p>
                      <p>
                        租用{service.item} 每小時HK${service.rate}
                        <div>(租用時間3小時起)</div>
                      </p>
                      <Link to={`/service/${service._id}`}><p className="link">查看更多</p></Link>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
          <a href="https://www.google.com.hk/maps/place/Yue+Xiu+Industrial+Building,+87+Hung+To+Rd,+Kwun+Tong/@22.3078295,114.2228021,17z/data=!3m1!4b1!4m5!3m4!1s0x34040144efe9d42b:0x761e4126bb8623d6!8m2!3d22.3077862!4d114.224979" target="_blank"><div className="map" style={{ backgroundImage: "url('/image_map.png')", backgroundSize: "cover" }}></div></a>
          <div className="sub-footer" style={{ backgroundImage: "url('/footer.png')", backgroundSize: "cover" }}>
            <div className="footer-wrap">
              <img onClick={() => window.scrollTo(0, 0)} className="go-to-top" src="/to-top.png" />
              <div>
                <h3>聯絡我們</h3>
                <div className="line-thru"></div>
                <h4>musicNext</h4>
                <p>Email: info@wsmmusic.com</p>
                <p>
                  香港九龍官塘鴻圖道87號越秀大廈2樓
                  <div>2/F, YUE XIU BLDG, 87 HUNG TO RD, KWUN TONG, KLN, HK</div>
                </p>
                <p>TEL: (+852) 3100-1111   FAX: (+852) 3105-3106</p>
              </div>
              <div>
                <h3>追蹤我們</h3>
                <div className="line-thru"></div>
                <div className="social">
                  <a href="https://www.facebook.com/musicNEXT.hk" target="_blank"><img src="/icons/fb.png" alt="" /></a>
                  <a href="https://www.instagram.com/musicnexthk/" target="_blank"><img src="/icons/ig.png" alt="" /></a>
                  <a href="https://www.youtube.com/user/musicnext" target="_blank"><img src="/icons/yt.png" alt="" /></a>
                </div>
              </div>
            </div>
          </div>
        </div>
        }
      </div>
    );
  }
}

const HomeContainer = createContainer(({ params }) => {
  const { filter } = params;

  let sub;

  Meteor.subscribe('authors');
  Meteor.subscribe('services');
  Meteor.subscribe('home');

  if(filter) {
    sub = Meteor.subscribe('news', filter);
  } else {
    sub = Meteor.subscribe('news', 'all');
  }

  const loading = !sub.ready();

  $.getScript("/js/jquery.timeago.js");
  //the above code is to add 'n hours ago' to the DOM
  //console.log(News.find({}).fetch().sort((a, b) => (b.createdAt - a.createdAt)));
  return {
    data: News.find({}).fetch().sort((a, b) => (b.createdAt - a.createdAt)),
    artists: AuthorID.find().fetch(),
    services: Services.find().fetch(),
    home: HomeData.findOne(),
    loading,
  }
}, Home);

// const CardListContainer = createContainer(({ params }) => {
//   const { typeId } = params;
//   Meteor.subscribe('news', typeId);
//   return {
//     news: News.find({}).fetch(),
//   }
// }, CardList);

export default HomeContainer;
