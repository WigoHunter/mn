import { Meteor } from 'meteor/meteor';
import { Mongo, MongoInternals } from 'meteor/mongo';
import { FilesCollection } from 'meteor/ostrio:files';
import Grid from 'gridfs-stream';
import fs from 'fs';

export const News = new Mongo.Collection('news');
export const AuthorID = new Mongo.Collection('authors');
export const Services = new Mongo.Collection('services');
export const HomeData = new Mongo.Collection('homedata');

let gfs;
if(Meteor.isServer) {
  gfs = Grid(
    MongoInternals.defaultRemoteCollectionDriver().mongo.db,
    MongoInternals.NpmModule
  );
}

export const Images = new FilesCollection({
  collectionName: 'Images',
  allowClientCode: false,
  onBeforeUpload: (file) => {
    if(file.size <= 10485760 && /png|jpg|jpeg/i.test(file.extension)) {
      return true;
    } else {
      return 'Please upload image with size equal or less than 10MB';
    }
  },

  onAfterUpload(image) {
    Object.keys(image.versions).forEach(versionName => {
      const metadata = {
        versionName,
        imageId: image._id,
        storedAt: new Date()
      };

      const writeStream = gfs.createWriteStream({
        filename: image.name,
        metadata
      });

      fs.createReadStream(image.versions[versionName].path).pipe(writeStream);

      writeStream.on('close', Meteor.bindEnvironment(file => {
        const property = `versions.${versionName}.meta.gridFsFileId`;

        this.collection.update(image._id, {
          $set: {
            [property]: file._id.toString()
          }
        });

        this.unlink(this.collection.findOne(image._id), versionName);
      }));
    });
  },

  interceptDownload(http, image, versionName) {
    const _id = (image.versions[versionName].meta || {}).gridFsFileId;

    if(_id) {
      const readStream = gfs.createReadStream({ _id });
      readStream.on('error', err => {throw err;});
      readStream.pipe(http.response);
    }

    return Boolean(_id);
  },

  onAfterRemove(images) {
    images.forEach(image => {
      Object.keys(image.versions).forEach(versionName => {
        const _id = (image.versions[versionName].meta || {}).gridFsFileId;
        if(_id) gfs.remove({ _id }, err => { if(err) throw err; });
      });
    });
  }
});

if(Meteor.isServer) {
  Images.denyClient();
}
