import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

export const ContactMessages = new Mongo.Collection('ContactMessages');
