import React from 'react';
import { Router, Route, browserHistory, IndexRoute } from 'react-router';

import App from './ui/App.js';
import HomeContainer from './ui/Home.js';
import ArtistContainer from './ui/Artists.js';
import ProductContainer from './ui/Products.js';
import SingleArtistContainer from './ui/SingleArtist.js';
import { NewsContainer } from './ui/News.js';
import SingleNewsContainer from './ui/SingleNews.js';
import AboutContainer from './ui/About.js';
import ContactUsContainer from './ui/Contact.js';
import ServiceContainer, { Links } from './ui/Services.js';
import SingleServiceContainer from './ui/SingleService.js';
import AdminPageContainer, { CMSArtists, CMSNews, CMSAddNews, CMSAddArtist, CMSHome } from './ui/AdminPage.js';

export const renderRoutes = () => (
  <Router onUpdate={() => window.scrollTo(0, 0)} history={browserHistory}>
    <Route path="/admin" component={AdminPageContainer}>
      <Route path="cms/home" component={CMSHome} />
      <Route path="cms/artists" component={CMSArtists} />
      <Route path="cms/artists/new" component={CMSAddArtist} />
      <Route path="cms/news" component={CMSNews} />
      <Route path="cms/news/new" component={CMSAddNews} />
    </Route>
    <Route path="/" component={App}>
      <IndexRoute component={HomeContainer} />
      <Route path="admin" component={AdminPageContainer} />
      <Route path="/news(/:filter)">
        <Route path="page/:page" component={NewsContainer} />
      </Route>
      <Route path="/newsfeed/:id" component={SingleNewsContainer} />
      <Route path="/about" component={AboutContainer} />
      <Route path="artists" component={ArtistContainer} />
      <Route path="products" component={ProductContainer} />
      <Route path="artist/:id" component={SingleArtistContainer} />
      <Route path="links" component={Links} />
      <Route path="services" component={ServiceContainer} />
      <Route path="service/:id" component={SingleServiceContainer} />
      {/*<Route path="/:filter" component={HomeContainer} />*/}
      <Route path="contact" component={ContactUsContainer} />
    </Route>

  </Router>
);

/*
const CardListContainerWithoutFilter = () => (
  <CardListContainer params={{typeId: "all" }}/>
)

const CardListContainerWithFilter = (props) => (
  <CardListContainer params={{typeId: props.params.filter}}/>
)
*/
