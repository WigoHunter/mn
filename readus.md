Ken 0.5
Ken 5
Ken 4.5
Ken 3.5   --Added backend author profile support, added 'n hour ago' according to 'CreatedAt', and fixed jQuery dropdown bug
Kevin 2   --project setup
Kevin 4   --frontend of MusicNext Home
Kevin 3   --Merged and Changed the Containers Structure a bit
Kevin 1   --Add loading effect (loader image to be changed)
Kevin 4   --Update all the css to the new design
Ken 2.5   --Footer
Kevin 2.5 --Finished /artists and artists in Home page
Kevin 2   --Half-done SingleArtist, fix container issue
Kevin 1   --Finished SingleArtist
Kevin 1   --NewsPage setup
Kevin 2   --Finished paging and half-finished SingleNews (Except for FB plugin)
Kevin 1   --Finished FB comments plugin & SingleNews page
Ken 5     --added the 'LargeVCard' thing and its respective backend logic
Kevin 5   --Finished About page & fix some bugs in News
Kevin 4   --Finished Services and SingleService pages; update Home
Kevin 1   --Fixed some css
Ken 4     --Worked on the Contact Us page
Ken 1     --Added the Messagebox popup after sending the contact us message
Kevin 1   --Responsive on home page and nav
Kevin 1   --Responsive on Services & Artists
Kevin 1   --Responsive on AboutUS & ContactForm
Kevin 2   --Responsive menu
Kevin 0.5 --Responsive single news
Kevin 0.5 --rwd newslist
Kevin 0.5 --rwd done
Kevin 2   --CMS routers and component setups
Kevin 1   --textinput component, control
Kevin 1   --CMS backend
Kevin 1   --CMS functions
Kevin 2   --CMS Meteor File
Kevin 2   --CMS half-done
Kevin 2   --CMS
Kevin 3   --Fixed issues & contents
Kevin 1   --CMS upload into GridFS
Kevin 1   --Product page
Kevin 1.5 --Artist-related News and Slicker
Kevin 2   --Frontend complete
Kevin 6   --CMS almost done
Kevin 1   --CMS Home component and router
