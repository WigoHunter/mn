// Currently Under Meteor Router this is causing error due to loading more than once
// Using 'Global' JSinjected div to determine whether injected or not

  if ( $( "#SlideList" ).length ) {
      var slideIndex = 0;
      if ($( "#Injected" ).length != 1){
        $( "#JSinjected" ).append("<div id=Injected></div>");
        showSlides();
      }
  }

  function showSlides() {
      // console.log($( "#SlideList" ).length);
      // console.log($( "#Injected").length);

      if ( $( "#SlideList" ).length ) {
        var i;
        var slides = document.getElementsByClassName("individual-slide");
        var dots = document.getElementsByClassName("dot");
        for (i = 0; i < slides.length; i ++) {
         slides[i].style.display = "none";
          }
        slideIndex ++;
        if (slideIndex > slides.length) {slideIndex = 1}
        for (i = 0; i < dots.length; i++) {
          dots[i].className = dots[i].className.replace(" active", "");
        }
        slides[slideIndex-1].style.display = "block";
        dots[slideIndex-1].className += " active";
        }

      setTimeout(showSlides, 3000); // Change image every 3 seconds
  }
