import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import './db.js';
import '../imports/startup/server/mail-url.js';
import { Email } from 'meteor/email';

import { News, AuthorID, Services, HomeData } from '../imports/api/getnews.js';

Meteor.startup(() => {

  if(Meteor.users.find().fetch().length == 0) {
    Accounts.createUser({
      username: "admin",
      password: "82278227",
    });
  }

  const data = {
    from: 'guy@vibecreation.com',
    to: 'mattandkevin1060@gmail.com',
    subject: 'title',
    text: 'test',
  };

  // Meteor.call('send.email', data);

  if (AuthorID.find().fetch().length == 0)
  {
    console.log("Push initial testing Author data (to Server DB).")

    const authors = [{
      _id: "1",
      name: "陳詩欣 Eunice Chan",
      chiName: "陳詩欣",
      engName: "Eunice Chan",
      birthday: "12月29日",
      height: "168CM",
      star: "山羊座",
      lang: "廣東話、國語、英文",
      interest: "MMA、跑步、瑜伽、羽毛球",
      instrument: "鋼琴",
      school: "香港和專設計學院 時裝商貿畢業",
      pic: "/artists/l01.png",
      spic: "/artists/s01.png",
      cover: "/artists/pic01.png",
      description: "陳詩欣 Eunice 畢業於香港知專設計學院，主修Fashion Business。2010年參加HKDI Fashion Competition Performance榮獲冠軍，隨後被Jam Cast發掘並簽約成為旗下模特兒，正式開始模特兒工作。",
      story: "陳詩欣 Eunice 畢業於香港知專設計學院，主修Fashion Business。2010年參加HKDI Fashion Competition Performance榮獲冠軍，隨後被Jam Cast發掘並簽約成為旗下模特兒，正式開始模特兒工作。憑甜美笑容及平易近人的活潑開朗性格，獲得不同廣告商的青睞，廣告有能得利軟糖，果汁橙，Timberland, LG, 伊利優酸乳，PizzaHut, 和路雪糕微電影等等。"
    }, {
      _id: "2",
      name: "張子丰 Fred Cheung",
      chiName: "張子丰",
      engName: "Fred Cheung",
      birthday: "12月29日",
      height: "168CM",
      star: "山羊座",
      lang: "廣東話、國語、英文",
      interest: "跑步、瑜伽、羽毛球",
      instrument: "鋼琴",
      school: "香港和專設計學院 時裝商貿畢業",
      pic: "/artists/l02.png",
      spic: "/artists/s02.png",
      cover: "/artists/pic01.png",
      description: "張子丰的執導作品大多大膽破格，充滿個人特色，例如「死亡三部曲」的黑暗主題，亦有喜劇系列的《那夜凌晨，我坐上了大埔開往旺角的GoGoVan》。除自己作品外，亦多為其他歌手及唱片公司執導或監製MV。",
      story: "張子丰的執導作品大多大膽破格，充滿個人特色，例如「死亡三部曲」的黑暗主題，亦有喜劇系列的《那夜凌晨，我坐上了大埔開往旺角的GoGoVan》。除自己作品外，亦多為其他歌手及唱片公司執導或監製MV。",
    }, {
      _id: "3",
      name: "陳曉琪 Kellyjackie",
      chiName: "陳曉琪",
      engName: "Kellyjackie",
      birthday: "12月29日",
      height: "168CM",
      star: "山羊座",
      lang: "廣東話、國語、英文",
      interest: "MMA、跑步、瑜伽、羽毛球",
      instrument: "鋼琴",
      school: "香港和專設計學院 時裝商貿畢業",
      pic: "/artists/l03.png",
      spic: "/artists/s03.png",
      cover: "/artists/pic01.png",
      description: "2011年5月加盟唱片公司musicNEXT的唱作歌手。現任香港電台『警訊』主持，逢星期六 7pm於TVB播放。現時新碟亦已經密鑼緊鼓籌備當中，希望憑藉豐富的創作和全新的形象讓大家耳目一新。",
      story: "2011年5月加盟唱片公司musicNEXT的唱作歌手。現任香港電台『警訊』主持，逢星期六 7pm於TVB播放。現時新碟亦已經密鑼緊鼓籌備當中，希望憑藉豐富的創作和全新的形象讓大家耳目一新。",
    }, {
      _id: "4",
      name: "曹敏寶 Abo",
      chiName: "曹敏寶",
      engName: "Abo",
      birthday: "12月29日",
      height: "168CM",
      star: "山羊座",
      lang: "廣東話、國語、英文",
      interest: "MMA、跑步、瑜伽、羽毛球",
      instrument: "鋼琴",
      school: "香港和專設計學院 時裝商貿畢業",
      pic: "/artists/l04.png",
      spic: "/artists/s04.png",
      cover: "/artists/pic01.png",
      description: "曹敏寶，籍貫上海，美籍華人，香港歌手，於2007年參加有線電視舉辦的線動真音樂全港公開賽2007獲得季軍，曾經在有線娛樂台擔任節目主持，於2011年出道為歌手。其胞姊為藝人曹敏莉。",
      story: "曹敏寶，籍貫上海，美籍華人，香港歌手，於2007年參加有線電視舉辦的線動真音樂全港公開賽2007獲得季軍，曾經在有線娛樂台擔任節目主持，於2011年出道為歌手。其胞姊為藝人曹敏莉。",
    }, {
      _id: "5",
      name: "歌莉雅 Gloria",
      chiName: "歌莉雅",
      engName: "Gloria",
      birthday: "12月29日",
      height: "168CM",
      star: "山羊座",
      lang: "廣東話、國語、英文",
      interest: "MMA、跑步、瑜伽、羽毛球",
      instrument: "鋼琴",
      school: "香港和專設計學院 時裝商貿畢業",
      pic: "/artists/l05.png",
      spic: "/artists/s05.png",
      cover: "/artists/pic01.png",
      description: "歌莉雅，香港音樂人，曾為獨立唱作人，現為musicNEXT旗下女歌手，以推出HIFI碟為主。歌莉雅曾為多個獨立電影及廣告配樂，亦曾為演唱會及廣告做和聲演出。2014年7月推出新專輯《Our Moments》。",
      story: "歌莉雅，香港音樂人，曾為獨立唱作人，現為musicNEXT旗下女歌手，以推出HIFI碟為主。歌莉雅曾為多個獨立電影及廣告配樂，亦曾為演唱會及廣告做和聲演出。2014年7月推出新專輯《Our Moments》。",
    }];

    for(let i = 0; i < authors.length; i++) {
      AuthorID.insert(authors[i]);
    }
  }

  if(HomeData.find().fetch().length == 0) {
    const data = {
      sliders: [
        {
          pic: 'https://i.ytimg.com/vi/RTMnQrs2zRQ/maxresdefault.jpg',
          title: '歌莉雅 Gloria - 順',
          time: '08/12/2016',
          excerpt: 'Hello 大家好，我係Gloria歌莉雅。12年前我出了第一張的原創專輯《Good-morning gloria》我覺得而家係時候...',
        },
        {
          pic: 'https://i.ytimg.com/vi/RTMnQrs2zRQ/maxresdefault.jpg',
          title: '歌莉雅 Gloria - 順',
          time: '08/12/2016',
          excerpt: 'Hello 大家好，我係Gloria歌莉雅。12年前我出了第一張的原創專輯《Good-morning gloria》我覺得而家係時候...',
        },
        {
          pic: 'https://i.ytimg.com/vi/RTMnQrs2zRQ/maxresdefault.jpg',
          title: '歌莉雅 Gloria - 順',
          time: '08/12/2016',
          excerpt: 'Hello 大家好，我係Gloria歌莉雅。12年前我出了第一張的原創專輯《Good-morning gloria》我覺得而家係時候...',
        },
      ],
      ads: [
        'http://www.hksingerchannel.com/images/banner_sing.jpg',
        'http://www.hksingerchannel.com/images/banner_sing.jpg',
        'http://www.hksingerchannel.com/images/banner_sing.jpg'
      ],
      youtube: 'https://www.youtube.com/embed/4b3UYvJjPMk?ecver=1',
    };

    HomeData.insert(data);
  }

  if(Services.find().fetch().length == 0) {
    const data = [{
      item: "綵排室",
      title: "租用綵排室 提供最好的專業服務",
      description: "環星娛樂有限公司一向在音樂推廣的領域中精益求精，完善自我，推己及人。佔地逾千呎之「綵排室」及「錄音室」，配備精良，歡迎個人、學校或團體租用。",
      rate: "600",
      pic: "/service-rehearsal.png",
      env: ["/room.png", "/room.png", "/room.png", "/room.png", "/room.png", "/room.png", "/room.png", "/room.png", "/room.png"],
      equipments: [
        "Ludwig Drum Set x1",
        "Hartke A100 Bass Amplifier x1",
        "Fender Hot Rod Deluxe III x1",
        "Fender Blues - Deluxe Reissue x1",
        "Yamaha P200 88 Key x1",
        "Yamaha Motif xF8 88 Key x1",
        "Yamaha Motif xS8 88 Key x1",
        "Kong Triton 66 Key x1",
        "Shure Beta SM58A x6",
        "Fold Back Monitor x3",
        "Avion x10",
      ]
    }, {
      item: "錄音室",
      title: "租用錄音室 提供最好的專業服務",
      description: "環星娛樂有限公司一向在音樂推廣的領域中精益求精，完善自我，推己及人。佔地逾千呎之「綵排室」及「錄音室」，配備精良，歡迎個人、學校或團體租用。",
      rate: "500",
      pic: "/service-mic.png",
      env: ["/room.png", "/room.png", "/room.png"],
      equipments: [
        "Ludwig Drum Set x1",
        "Hartke A100 Bass Amplifier x1",
        "Fender Hot Rod Deluxe III x1",
        "Fender Blues - Deluxe Reissue x1",
        "Yamaha P200 88 Key x1",
        "Yamaha Motif xF8 88 Key x1",
        "Yamaha Motif xS8 88 Key x1",
        "Kong Triton 66 Key x1",
        "Shure Beta SM58A x6",
        "Fold Back Monitor x3",
        "Avion x10",
      ]
    }];

    for(let i = 0; i < data.length; i++) {
      Services.insert(data[i]);
    }
  }



  if(News.find().fetch().length == 0) {

    console.log("Push initial testing News data (to Server DB).");

    const data = [{
      pic: "https://www.hksingerchannel.com/images/interview/102.jpg",
      type: "musics",
      title: "Gloria歌莉雅 娛樂新聞專訪",
      author_id: "1",
      createdAt: new Date(),
      excerpt: "12年前，一位對音樂充滿熱誠的少女Gloria勇闖香港樂壇，推出《Good Morning Gloria》專輯並一手包辦大部分曲詞...",
      story: "12年前，一位對音樂充滿熱誠的少女Gloria勇闖香港樂壇，推出《Good Morning Gloria》專輯並一手包辦大部分曲詞，自作自唱。儘管她的唱片沒有大熱，但仍努力不懈繼續創作，接二連三推出了好幾張唱片。商業世界始終是現實的，Gloria在理想與生活之間努力堅持，路儘管艱辛，但她從沒放棄自己的音樂夢。皇天不負有心人，她的美麗聲線得到不少天王巨星的賞識，有機會屢踏紅館舞台，先後在葉蒨文、楊千嬅、王傑、鄭秀文、許志安、甄妮、劉家昌及關菊英的演唱會上擔任和音，日漸在百家爭鳴的樂壇鑽出頭來。到了2012年，Gloria的事業轉捩點來臨了。有唱片公司邀請她推出重唱別人的歌的HIFI發燒天碟專輯，並以歌莉雅的名字再戰樂壇。當時翻唱熱潮尚未成熟，發燒又碟市場也只在萌芽階段。誰不知這張《My Voice Story》專輯備受青睞，叫好又叫座，還奪得IFPI唱片銷量大獎，使歌莉雅在音樂界贏得HIFI碟小天后的美譽。接下來她每年都推出新的HIFI碟，並以不同主題向經典好歌致敬，包括樂隊歌曲、四大天王的歌曲及八、九十年代膾炙人口的男女歌手名作，唱片銷量節節上升，支持者數目與日俱增。"
    }, {
      pic: "http://hk.on.cc/hk/bkn/cnt/entertainment/20151113/photo/bkn-20151113160544793-1113_00862_001_04b.jpg?20151113165756",
      type: "musics",
      title: "CASH週年晚宴暨金帆音樂獎頒獎典禮",
      author_id: "2",
      createdAt: new Date(),
      excerpt: "Eunice陳詩欣今天晚上在香港會議展覽中心，出席<CASH週年晚宴暨金帆音樂獎頒獎典禮>，謝謝大會邀請...",
      story: "Eunice陳詩欣今天晚上在香港會議展覽中心，出席<CASH週年晚宴暨金帆音樂獎頒獎典禮>，謝謝大會邀請！談到頒獎禮時，Eunice即自爆非常喜歡天后容祖兒，更視對方為自己的女神，但入行後都沒有機會見到對方，笑言:「以前幻想過做歌手拎到新人獎，如果祖兒頒獎畀我就好喇，所以好期待會喺頒獎禮見到佢！」她亦表示已經為自己定下明年的目標，雖然沒有跳舞底子，但想挑戰自己成為跳唱歌手:「見到Super Girls咁勁，我都要改進吓！」在旁的Fred就透露推出了有關死亡主題的歌曲，更拍攝了MV，不過因MV情節及形象太恐怖而被電視台禁播，所以覺得很可惜，但同時都要感謝周國賢：「因為MV嘅形象需要，所以周國賢就介紹咗個特技化妝師畀我先可以順利拍攝呢次MV。」至於問到對頒獎禮可有信心，Eunice說:「有嘅，但覺得獎項係bonus，見到祖兒最開心！」而Fred則指自己沒有想過奪得任何獎項。",
    }, {
      pic: "https://www.post76.com/wordpress/wp-content/uploads/2016/08/2016-%E5%BD%B1%E9%9F%B3%E5%B1%95%E7%89%B9%E5%88%A5%E5%A0%B4%E3%80%8A%E6%AD%8C%E8%8E%89%E9%9B%85%E9%9D%9A%E8%81%B2%E9%9F%B3%E6%A8%82%E5%88%86%E4%BA%AB%E6%9C%83%E3%80%8B.jpg",
      type: "interviews",
      title: "索取 2016 視聽展內 環星 x 大昌《歌莉雅靚聲音樂分享會》特別場門券",
      author_id: "3",
      createdAt: new Date(),
      excerpt: "明天 2016 年 8 月 7 日 (星期日) 到【2016 香港高級視聽展】環星音樂 K08 攤位搶購歌莉雅 CD...",
      story: "明天 2016 年 8 月 7 日 (星期日) 到【2016 香港高級視聽展】環星音樂 K08 攤位搶購歌莉雅 CD、SACD 或 黑膠，可獲取同日下午 3:00 於 S224 大昌音響高級發燒音響房內舉行的《歌莉雅靚聲音樂分享會》，到時 Gloria 除會分享製作靚聲專輯的感受外，更會即場獻唱佢嘅靚歌，會後更可索取 Gloria 簽名。門票# 限量名額 15 位 (共 30 張飛)，以每人單一選購歌莉雅大碟+ 計算，數量有限，送完即止。"
    }, {
      pic: "http://hk.on.cc/cnt/entertainment/20160120/photo/bkn-20160120004823527-0120_00862_001_01b.jpg?20160120033149",
      type: "events",
      title: "Kellyjackie食齋Keep‬ Fit",
      author_id: "2",
      createdAt: new Date(),
      excerpt: "歌手Kellyjackie近年愛上入廚，閒時煮番三四味同家人分享，最拿手就是「蕃茄炒蛋」，她自豪說：...",
      story: "歌手Kellyjackie近年愛上入廚，閒時煮番三四味同家人分享，最拿手就是「蕃茄炒蛋」，她自豪說：「係婆婆細個煮畀我食嘅食譜，婆婆食完都讚，話真係佢煮嗰味道。」身為佛教徙的Kellyjackie有意食全齋，她說：「因為媽咪係食全素，一直Keep住一日食一餐齋，依家開始想一日兩餐係齋，食少啲肉又環保又可以減肥。」Kellyjackie表示自己煮飯較出外食飯健康及平，她說：「冇味精，又可以少油、少糖同少鹽，會健康好多。」問到有否為男友下廚？Kellyjackie強調現時單身，她說：「煮畀細路食多，當然都試過煮畀男朋友食，佢都大讚話畀滿分我，至少佢冇食到頭暈身興。」"
    }, {
      pic: "/pic.jpg",
      type: "shows",
      title: "‎陳詩欣食招牌燉奶唔怕殘忍",
      author_id: "3",
      createdAt: new Date(),
      excerpt: "新人歌手陳詩欣（Eunice）近日終於忙完微電影的拍攝，在煞科當日更同工作人員拉大隊去了位於中環的Little...",
      story: "新人歌手陳詩欣（Eunice）近日終於忙完微電影的拍攝，在煞科當日更同工作人員拉大隊去了位於中環的Little Twins Star主題點心店慶功。Eunice見到外表吸引的卡通點心當然「相機食先」放上網「呃Like」，引來粉絲羨慕之餘，亦有人笑她「殘忍」：「因為個樣好靚，有Little Twins Star公仔喺上面，啲朋友見到就話你咁殘忍o架，人哋咁得意你都食。」在眾多點心中，Eunice就最推薦招牌菜燉奶：「個燉奶係北海道牛奶整，好重牛奶味，一定要趁熱食；有個流沙奶黃包都好好食，但就要小心啲，因為一咬就會咇汁，唔小心就會辣到個嘴！」而傳統的港式點心中，Eunice亦激讚蝦餃燒賣夠正宗，其中燒賣更是揼本之作，原隻帶子放面蒸，認真足料！"
    }, {
      pic: "http://hk.on.cc/hk/bkn/cnt/entertainment/20160107/photo/bkn-20160107043142828-0107_00862_001_01p.jpg?20160107083851",
      type: "lives",
      title: "KellyJackie要男仔開聲示愛",
      author_id: "3",
      createdAt: new Date(),
      excerpt: "KellyJackie要男仔開聲示愛‬(on.cc東網 7/1/2016)【on.cc東網專訊】歌手KellyJackie（KJ）早前出席...",
      story: "KellyJackie要男仔開聲示愛‬(on.cc東網 7/1/2016)【on.cc東網專訊】歌手KellyJackie（KJ）早前出席活動時，指剛剛拍攝完微電影，之後就會推出新歌。談到新歌，KJ表示新歌主題是圍繞結婚，更笑言最近經常收到「紅色炸彈」，而身邊有很多朋友都結了婚。問到她感情生活如何，KJ說：「隨緣啦，不過有喜歡嘅人。（會女追男？）可以做暗示嗰個，但開口就始終要男仔。」還透露在聖誕節都有親自整曲奇餅給朋友，都有送給心儀的對象。"
    }, {
      pic: "http://hk.on.cc/hk/bkn/cnt/entertainment/20160103/photo/bkn-20160103064806495-0103_00862_001_03b.jpg?20160103064815",
      type: "events",
      title: "Eunice陳詩欣 娛樂新聞報導",
      author_id: "3",
      createdAt: new Date(),
      excerpt: "<Eunice陳詩欣 娛樂新聞報導>(太陽報 4/1/2016)歌手陳詩欣日前與粉絲慶生時一度感觸落淚，她說：...",
      story: "<Eunice陳詩欣 娛樂新聞報導>(太陽報 4/1/2016)歌手陳詩欣日前與粉絲慶生時一度感觸落淚，她說：「有啲Fans喺我做Model時就跟住我，同埋我最估唔到嘅係有個內地Fans嚟。」"
    }, {
      pic: "http://cn.on.cc/cnt/entertainment/20151224/photo/bkn-20151224023555168-1224_00862_001_02b.jpg?20151224024956",
      type: "musics",
      title: "陳詩欣操拳唔怕嚇走追求者‬",
      author_id: "3",
      createdAt: new Date(),
      excerpt: "新人陳詩欣（Eunice）日前出席微電影首映，穿上連身短裙的她右膊及膝頭分別有瘀痕，Eunice透露膊上...",
      story: "新人陳詩欣（Eunice）日前出席微電影首映，穿上連身短裙的她右膊及膝頭分別有瘀痕，Eunice透露膊上是胎記，她說：「近日拍攝微電影，當中有好多打鬥場面，撞瘀、擦傷少不免，不過大家都會就住嚟，都唔希望整到對方。」談到有女藝人參加MMA（綜合格鬥）比賽，當中有人更被打到「花面貓」，有練拳的Eunice被問到會否參加MMA比賽？她笑說：「要進步多啲先考慮，因為要好大體力。（唔驚嚇怕追求者？）咁我都有溫柔一面。」問到是否已有男友？Eunice笑笑口說：「嘩！咁樣試我。」"
    }, {
      pic: "http://cn.on.cc/hk/bkn/cnt/entertainment/20151102/photo/bkn-20151102023031484-1102_00862_001_02b.jpg?20151102114828",
      type: "lives",
      title: "‎Kellyjackie武動雙節棍傷指公",
      author_id: "3",
      createdAt: new Date(),
      excerpt: "歌手陳曉琪（Kellyjackie）、陳詩欣和張子丰日前接受東網電視《娛樂onShow》訪問，Kellyjackie自言是...",
      story: "歌手陳曉琪（Kellyjackie）、陳詩欣和張子丰日前接受東網電視《娛樂onShow》訪問，Kellyjackie自言是「女漢子」，近年更習武學雙節棍。她又大爆與閨蜜吳若希私下會以老公、老婆相稱，十分Sweet！陳詩欣則笑指張子丰是「小白臉」：「唔係我杯茶！」　陳詩欣、Kellyjackie和和張子丰日前擔任東網電視《娛樂onShow》嘉賓，接受主持李文珊（Sherman）訪問。雖然Kellyjackie外表斯文，但她坦言是「女漢子」，近年更習武學雙節棍：「我一試玩綿造嘅雙節棍，我就棍不離手！真係上咗癮！」她又透露習武留下的彪炳戰績：「我同拍檔練雙打表演時，唔小心畀佢打中手指公，我即刻覺得成隻手指公冇咗！」雖然習武形象Man爆，但Kellyjackie自言仍有女性的一面，是剛柔並重。"
    }, {
      pic: "http://cn.on.cc/hk/bkn/cnt/entertainment/20151212/photo/bkn-20151212144505799-1212_00862_001_01b.jpg?20151212161304",
      type: "musics",
      title: "《娛樂onShow》：KJ陳詩欣張子丰教你防狼術",
      author_id: "3",
      createdAt: new Date(),
      excerpt: "【on.cc東網專訊】 在今集《娛樂onShow》，Kellyjackie、陳詩欣和張子丰即席角色扮演，學授女士們...",
      story: "【on.cc東網專訊】 在今集《娛樂onShow》，Kellyjackie、陳詩欣和張子丰即席角色扮演，學授女士們防狼術！另外，三人更會分別大講他們的奇趣事，想知道的話，記得留意《娛樂onShow》！"
    }, {
      pic: "http://hk.on.cc/hk/bkn/cnt/entertainment/20151215/photo/bkn-20151215033119864-1215_00862_001_01p.jpg?20151215124831",
      type: "events",
      title: "陳詩欣狂收性騷擾電話",
      author_id: "3",
      createdAt: new Date(),
      excerpt: "【on.cc東網專訊】 女歌手陳詩欣（Eunice）憑甜美笑容被網友封為新一代女神，理應不乏追求者，但她卻...",
      story: "【on.cc東網專訊】 女歌手陳詩欣（Eunice）憑甜美笑容被網友封為新一代女神，理應不乏追求者，但她卻透露感情上交白卷：「入咗行係識多咗新朋友，不過真係冇人追。」事業型的Eunice表示暫時將工作放在第一位，她又透露近日慘受電話騷擾，對方以隱藏號碼來電，當她接聽時對方即大講淫穢粗口：「我一聽，佢就講鹹濕嘢同粗口，明顯係性騷擾。（報警？）冇，即刻Cut線，同埋唔聽冇來電嘅電話。」問到對方是否歌迷？她說：「可能性好低，我啲歌迷好乖又有禮貌。」"
    }, {
      pic: "http://cn.on.cc/hk/bkn/cnt/entertainment/20151206/photo/bkn-20151206023128896-1206_00862_001_02b.jpg?20151206102746",
      type: "events",
      title: "‎歌莉雅新片重拾童真",
      author_id: "3",
      createdAt: new Date(),
      excerpt: "歌手歌莉雅（Gloria）日前出席音響活動，並獲大會邀請在台上自彈自唱。她表示正忙於籌備專輯...",
      story: "歌手歌莉雅（Gloria）日前出席音響活動，並獲大會邀請在台上自彈自唱。她表示正忙於籌備專輯：「請咗唔同音樂人去幫我，自己都好期待，好想睇到有咩火花！」與唱片公司約滿的她指現正處於接洽階段：「依家傾緊續約問題，同公司一路以來都合作得好開心。」另外，Gloria透露有份主演的電影即將上映，令她萬般期待：「之前拍咗一部兒童電影，係講中港矛盾，我飾演一個音樂老師，都幾開心同咁多小朋友合作，令自己變得好有童真！」"
    }];

    for(let i = 0; i < data.length; i++) {
      News.insert(data[i]);
    }
  }
});

//pic: "/mn_im_news_01.png", type: "star", title: "CASH週年晚宴暨金帆音樂獎頒獎典禮", timeago: "18 hours ago", author: { name: "陳詩欣 Eunice Chan", pic: "/mn_im_news_06.png", }, createdAt: new Date(), excerpt: "Eunice陳詩欣今天晚上在香港會議展覽中心，出席<CASH週年晚宴暨金帆音樂獎頒獎典禮>，謝謝大會邀請！",
