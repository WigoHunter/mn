import { Meteor } from 'meteor/meteor';
//import { Mongo } from 'meteor/mongo'; Unused import
import { Email } from 'meteor/email';

import { News, AuthorID, Services, HomeData, Images } from '../imports/api/getnews.js';

if (Meteor.isServer) {
  console.log("On Server! Publishing Images.");
  Meteor.publish('files.images.all', function imagePublish() {
    return Images.find().cursor;
  });

  Meteor.publish('home', function homePublish() {
    return HomeData.find({});
  })

  console.log("On Server! Publishing news.");
  Meteor.publish('news', function newsPublish(type, page) {
    console.log(`publishing page ${page}`);
    return type === "all" ? News.find({}) : News.find({ type: type });
  });

  Meteor.publish('authorNews', function publish(id) {
    return News.find({ 'author_id': id }, { limit: 5 });
  });
}

if (Meteor.isServer) {
  console.log("On Server! Publishing newsfeed.");
  Meteor.publish('newsfeed', function newsPublish(id) {
    return News.find({ _id: id });
  });

  Meteor.publish('recentnews', function recentnewsPublish() {
    return News.find({}, { sort: {createdAt: -1}, limit: 4 });
  })
}

if (Meteor.isServer) { //Currently not sure where slideshow sources come from
  console.log("On Server! Publishing slidenews.");
  Meteor.publish('slidenews', function slidenewsPublish() {
    return News.find({});
  });
}

if (Meteor.isServer) {
  Meteor.publish('services', function servicesPublish() {
    return Services.find({});
  });

  Meteor.publish('singleService', function singleServicePublish(id) {
    return Services.find({ _id: id });
  })
}

if (Meteor.isServer) {
  console.log("On Server! Publishing authors.");
  Meteor.publish('authors', function authorsPublish(){
    return AuthorID.find({});
  });

  Meteor.publish('allnews', function func() {
    return News.find({});
  });
}

Meteor.methods({
  'news.insert'(title, content, typeId, artistId, thumbnail) { //news title content, news type, the ID of artist and URL of thumbnail respectively

    //if(!this.userId) {
    //  throw new Meteor.Error('NOT AUTHORIZED');
    //}
    News.insert({
      title: title,
      createdAt: new Date(),
      type: typeId,
      excerpt: content,
      author_id: artistId,
      pic: thumbnail,
    });
  },

  'news.remove'(id) {
    News.remove(id);
  },

  'contact.send.comment' (name, email, title, content) {
    console.log("Send Comment Executed!");
    return true;
  },

  'hide.artist'(id) {
    const i = parseInt(id) + 1, query = `${i}`;
    let cur = AuthorID.findOne({ _id: query }).hide ? AuthorID.findOne({ _id: query }).hide : false;

    AuthorID.update(query, {
      $set: {
        hide: !cur,
      }
    });
  },

  'updateArtist' (id, data) {
    const i = parseInt(id) + 1, query = `${i}`;

    console.log(AuthorID.findOne({ _id: query }));

    console.log('-------');

    AuthorID.update(query, {
      $set: {
        name: data.name,
        chiName: data.chiName,
        engName: data.engName,
        birthday: data.birthday,
        height: data.height,
        star: data.star,
        lang: data.lang,
        interest: data.interest,
        instrument: data.instrument,
        school: data.school,
        description: data.description,
        story: data.story,
        facebook: data.facebook,
        youtube: data.youtube,
        instagram: data.instagram,
      }
    });

    return true;
  },

  'update.artist.pic'(id, path, ext) {
    const i = parseInt(id) + 1, query = `${i}`;
    AuthorID.update(query, {
      $set: {
        pic: `/cdn/storage/Images/${path}/original/${path}.${ext}`,
      }
    });
  },

  'update.artist.spic'(id, path, ext) {
    const i = parseInt(id) + 1, query = `${i}`;
    AuthorID.update(query, {
      $set: {
        spic: `/cdn/storage/Images/${path}/original/${path}.${ext}`,
      }
    });
  },

  'update.artist.cover'(id, path, ext) {
    const i = parseInt(id) + 1, query = `${i}`;
    console.log(path);
    AuthorID.update(query, {
      $set: {
        cover: `/cdn/storage/Images/${path}/original/${path}.${ext}`,
      }
    });
  },

  'update.news.pic'(id, path, ext) {
    News.update(id, {
      $set: {
        pic: `/cdn/storage/Images/${path}/original/${path}.${ext}`,
      }
    });
  },

  'update.homeslider'(id, data) {
    let sliders = HomeData.findOne().sliders;

    sliders[id].title = data.title;
    sliders[id].time = data.time;
    sliders[id].excerpt = data.excerpt;
    sliders[id].link = data.link;

    HomeData.update({}, {
      $set: {
        sliders: sliders,
      }
    });

    return true;
  },

  'update.home.yt'(link) {
    HomeData.update({}, {
      $set: {
        youtube: link,
      }
    });
  },

  'update.home.ads'(id, path, ext) {
    let ads = HomeData.findOne().ads;
    ads[id] = `/cdn/storage/Images/${path}/original/${path}.${ext}`;
    console.log('hit');

    HomeData.update({}, {
      $set: {
        ads: ads,
      }
    });

    return true;
  },

  'update.homecover.pic'(id, path, ext) {
    let sliders = HomeData.findOne().sliders;

    sliders[id].pic = `/cdn/storage/Images/${path}/original/${path}.${ext}`;

    console.log(sliders);

    HomeData.update({}, {
      $set: {
        sliders: sliders,
      }
    });
  },

  'update.news'(id, data) {
    News.update(id, {
      $set: data
    });

    return true;
  },

  'send.email'(data) {
    console.log(data);
    Email.send(data);
    // Email.send({
    //   from: 'guy@vibecreation.com',
    //   to: 'mattandkevin1060@gmail.com',
    //   subject: 'title',
    //   text: 'test',
    // });
    return true;
  },

  'add.news'(data) {
    console.log('inserting news...');
    News.insert(data);
  },

  'add.artist'(data) {
    console.log('inserting artist...');
    AuthorID.insert(data);
  }
});
